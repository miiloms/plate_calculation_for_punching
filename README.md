##Summary:
The desktop application for Engineer-design of Buildings. It calculate strong steel-concrete plates for punching. It has two mode- auto and manual mode. You will see how Asw steel  are allocated at punching location.

#Application interface:#
**Main window -**
Here you may enter your calculation data and choose mode-work of application (plate or plate+cap) and any more features.
![Screenshot_1.png](https://bitbucket.org/repo/4LMdKz/images/471833410-Screenshot_1.png)

  **Auto calculate parameters**

![Screenshot_2.png](https://bitbucket.org/repo/4LMdKz/images/3267853526-Screenshot_2.png)

  **General report window -**
Here you can see result of calculation and suggestions for error solution.

![Screenshot_3.png](https://bitbucket.org/repo/4LMdKz/images/1122798907-Screenshot_3.png)

  **Report window whith bottom  window part -**
This window show you how completed calculation with using Asw steel

![Screenshot_4.png](https://bitbucket.org/repo/4LMdKz/images/805172709-Screenshot_4.png)

  **Rendering  Asw steel allocation in the steel-concrete plate**

  ![Screenshot_5.png](https://bitbucket.org/repo/4LMdKz/images/2230058242-Screenshot_5.png)

   **Using technology:**

Windows Forms (C#)

   **IDE:**

MS Visual Studio 13