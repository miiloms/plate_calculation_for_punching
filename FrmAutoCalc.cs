﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Application_for_Calc_Punch_Building_
{
    public partial class FrmAutoCalc : Form
    {
        bool fixH;
        bool useAsw;
        double fixHcap;
        uint step;
        uint reserv;
        public static calcClicMethod delegCalcSolAuto;
        Slab slab_work;
        FrmStartWindow ParentWind;
        ProgressBar prgBar;
        public FrmAutoCalc(Slab _slab_work, FrmStartWindow _ParentWind)
        {
            InitializeComponent();
            this.nUD_Step.Increment = 25;
            this.nUD_Step.Value = 25;
            this.numUD_percent.Value = 5;
            this.numUD_percent.Maximum = 20;
            this.txtB_FixHCap.Enabled = false;
            this.slab_work = _slab_work;
            this.ParentWind = _ParentWind;
        }

        private void chBox_FixhCap_CheckedChanged(object sender, EventArgs e)
        {
            this.txtB_FixHCap.Enabled = this.chBox_FixhCap.Checked;
            if (this.chBox_FixhCap.Checked == true)
            {
                this.txtB_FixHCap.Text = "50";
                this.chBox_UseAsw.Checked = true;
                this.chBox_UseAsw.Enabled = false;
            }
            else
            {
                this.txtB_FixHCap.Text = "0";
                this.chBox_UseAsw.Checked = false;
                this.chBox_UseAsw.Enabled = true;
            } 
        }
        void GatheringDataForm()
        {
            fixH = this.chBox_FixhCap.Checked;
            useAsw = this.chBox_UseAsw.Checked;
            if (!double.TryParse(this.txtB_FixHCap.Text, out fixHcap)&&this.chBox_FixhCap.Checked)
                throw new Exception("Неверно введена высота капители!");
            step = (uint)this.nUD_Step.Value;
            reserv = (uint)this.numUD_percent.Value;   
        }
        void CreateProgressBar()
        {
            this.Size = new Size(this.Size.Width, this.Size.Height+25);
            this.prgBar = new ProgressBar();
            prgBar.Size = new Size(this.Size.Width, 25);
            prgBar.Location = new Point(0, this.Size.Height - 60);
            this.Controls.Add(prgBar);
        }
        void AutoSolving()
        {
            this.CreateProgressBar();
            int answ;
            int mod2=0;
            bool setCap = false;
            double Wcap=250;
            try
                {
                this.GatheringDataForm();
                while (true)
                {
                    this.prgBar.Increment(10);
                    this.ParentWind.AcceptData(setCap, this.useAsw, this.fixHcap, Wcap, reserv);
                    this.ParentWind.GatheringDataForm(this);
                    answ = this.slab_work.checkVsd_Vrdc();
                    if(! this.useAsw) //расчет без арматуры
                    switch (answ)
                    {
                        case 1: this.prgBar.Increment(100); FrmAutoCalc.delegCalcSolAuto(this, new EventArgs()); this.Close(); return;
                        case 0: setCap = true; fixHcap += step; break;
                        case -1: setCap = true; fixHcap += step; break;
                        case 2: setCap = true; Wcap += step; break;
                        case 3: setCap = true; fixHcap += step; break;
                        case 4: setCap = true; Wcap += step; break;
                        case 5: setCap = true; Wcap += step; fixHcap += step; break;
                        default: return;
                    }
                    if (this.useAsw) // расчет с арматурой
                        switch (answ)
                        {
                            case 1: this.prgBar.Increment(100); FrmAutoCalc.delegCalcSolAuto(this, new EventArgs()); this.Close(); return;
                            case 0: this.prgBar.Increment(100); FrmAutoCalc.delegCalcSolAuto(this, new EventArgs()); this.Close(); return;
                            case -1: setCap = true; fixHcap=(this.chBox_FixhCap.Checked)?fixHcap:fixHcap+=step; break;
                            case 2: setCap = true; Wcap += step; break;
                            case 3: this.prgBar.Increment(100); FrmAutoCalc.delegCalcSolAuto(this, new EventArgs()); this.Close(); return;
                            case 4: setCap = true; Wcap += step; break;
                            case 5: setCap = true; Wcap += step;
                                mod2++;
                                if(mod2%4==0)
                                    fixHcap = (this.chBox_FixhCap.Checked) ? fixHcap : fixHcap += step; 
                                break;
                            case 6: setCap = true;
                                if (chBox_FixhCap.Checked)
                                { MessageBox.Show("Невозможно установить Asw при толщине капители " + fixHcap.ToString(), "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;}
                                fixHcap += step;break;
                            default: return;
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }   
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            this.Location = new Point(this.Location.X, this.Location.Y - 275); 
            AutoSolving();
        }
    }
}
