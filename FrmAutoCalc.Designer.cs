﻿namespace Application_for_Calc_Punch_Building_
{
    partial class FrmAutoCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAutoCalc));
            this.chBox_UseAsw = new System.Windows.Forms.CheckBox();
            this.nUD_Step = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCalc = new System.Windows.Forms.Button();
            this.chBox_FixhCap = new System.Windows.Forms.CheckBox();
            this.numUD_percent = new System.Windows.Forms.NumericUpDown();
            this.txtB_FixHCap = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Step)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_percent)).BeginInit();
            this.SuspendLayout();
            // 
            // chBox_UseAsw
            // 
            this.chBox_UseAsw.AutoSize = true;
            this.chBox_UseAsw.Location = new System.Drawing.Point(15, 35);
            this.chBox_UseAsw.Name = "chBox_UseAsw";
            this.chBox_UseAsw.Size = new System.Drawing.Size(120, 17);
            this.chBox_UseAsw.TabIndex = 1;
            this.chBox_UseAsw.Text = "использовать Asw";
            this.chBox_UseAsw.UseVisualStyleBackColor = true;
            // 
            // nUD_Step
            // 
            this.nUD_Step.Location = new System.Drawing.Point(208, 55);
            this.nUD_Step.Name = "nUD_Step";
            this.nUD_Step.Size = new System.Drawing.Size(44, 20);
            this.nUD_Step.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Шаг подбора по размерам :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(258, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "мм";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(261, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Запас прочности:";
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(15, 103);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(75, 23);
            this.btnCalc.TabIndex = 10;
            this.btnCalc.Text = "Подобрать";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // chBox_FixhCap
            // 
            this.chBox_FixhCap.AutoSize = true;
            this.chBox_FixhCap.Location = new System.Drawing.Point(15, 12);
            this.chBox_FixhCap.Name = "chBox_FixhCap";
            this.chBox_FixhCap.Size = new System.Drawing.Size(202, 17);
            this.chBox_FixhCap.TabIndex = 11;
            this.chBox_FixhCap.Text = "фиксированная высота капители: ";
            this.chBox_FixhCap.UseVisualStyleBackColor = true;
            this.chBox_FixhCap.CheckedChanged += new System.EventHandler(this.chBox_FixhCap_CheckedChanged);
            // 
            // numUD_percent
            // 
            this.numUD_percent.Location = new System.Drawing.Point(208, 79);
            this.numUD_percent.Name = "numUD_percent";
            this.numUD_percent.Size = new System.Drawing.Size(44, 20);
            this.numUD_percent.TabIndex = 12;
            // 
            // txtB_FixHCap
            // 
            this.txtB_FixHCap.Location = new System.Drawing.Point(208, 10);
            this.txtB_FixHCap.Name = "txtB_FixHCap";
            this.txtB_FixHCap.Size = new System.Drawing.Size(44, 20);
            this.txtB_FixHCap.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(258, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "мм";
            // 
            // FrmAutoCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 131);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtB_FixHCap);
            this.Controls.Add(this.numUD_percent);
            this.Controls.Add(this.chBox_FixhCap);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nUD_Step);
            this.Controls.Add(this.chBox_UseAsw);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmAutoCalc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры авто подбора";
            ((System.ComponentModel.ISupportInitialize)(this.nUD_Step)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_percent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chBox_UseAsw;
        private System.Windows.Forms.NumericUpDown nUD_Step;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.CheckBox chBox_FixhCap;
        private System.Windows.Forms.NumericUpDown numUD_percent;
        private System.Windows.Forms.TextBox txtB_FixHCap;
        private System.Windows.Forms.Label label5;
    }
}