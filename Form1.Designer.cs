﻿namespace Application_for_Calc_Punch_Building_
{
    partial class FrmStartWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStartWindow));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsExit = new System.Windows.Forms.ToolStripMenuItem();
            this.расчетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMenuWoutCap = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMenuWithCap = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.grBox_LocationColumn = new System.Windows.Forms.GroupBox();
            this.pBox_Corner = new System.Windows.Forms.PictureBox();
            this.pBox_Center = new System.Windows.Forms.PictureBox();
            this.pBox_Outside = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.grBoxConfig = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tBoxMain_Asty = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tBoxMain_Astx = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tBox1_Ln = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tBox_Cy = new System.Windows.Forms.TextBox();
            this.tBox_Cx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCoverLayer = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tBox_acol = new System.Windows.Forms.TextBox();
            this.tBox_Hcap = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tBox_Hpl = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tBox_bcol = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cmBox_Diam_St = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cmBox_ClassSteel = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cmBox_classConcrete = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tBoxLoad = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tlStrStatL = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlSStatusLMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnCalcSol = new System.Windows.Forms.Button();
            this.btnAutoSol = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.chbUseAsw = new System.Windows.Forms.CheckBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnOpenFile = new System.Windows.Forms.ToolStripButton();
            this.tsBtnQickSave = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSaveAs = new System.Windows.Forms.ToolStripButton();
            this.tsBtnExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnMode = new System.Windows.Forms.ToolStripSplitButton();
            this.tsBtnModeSlab = new System.Windows.Forms.ToolStripMenuItem();
            this.tsBtnModeCap = new System.Windows.Forms.ToolStripMenuItem();
            this.rTextBoxComments = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.grBox_LocationColumn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Corner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Center)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Outside)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.grBoxConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.расчетToolStripMenuItem,
            this.помощьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(837, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsOpenFile,
            this.tsmSaveAs,
            this.tsSave,
            this.tsExit});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // tsOpenFile
            // 
            this.tsOpenFile.Name = "tsOpenFile";
            this.tsOpenFile.Size = new System.Drawing.Size(162, 22);
            this.tsOpenFile.Text = "Открыть";
            // 
            // tsmSaveAs
            // 
            this.tsmSaveAs.Name = "tsmSaveAs";
            this.tsmSaveAs.Size = new System.Drawing.Size(162, 22);
            this.tsmSaveAs.Text = "Сохранить как...";
            this.tsmSaveAs.Click += new System.EventHandler(this.tsmSaveData_Click);
            // 
            // tsSave
            // 
            this.tsSave.Name = "tsSave";
            this.tsSave.Size = new System.Drawing.Size(162, 22);
            this.tsSave.Text = "Сохранить";
            this.tsSave.Click += new System.EventHandler(this.tsSave_Click);
            // 
            // tsExit
            // 
            this.tsExit.Name = "tsExit";
            this.tsExit.Size = new System.Drawing.Size(162, 22);
            this.tsExit.Text = "Выйти";
            this.tsExit.Click += new System.EventHandler(this.tsExit_Click);
            // 
            // расчетToolStripMenuItem
            // 
            this.расчетToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMenuWoutCap,
            this.TSMenuWithCap});
            this.расчетToolStripMenuItem.Name = "расчетToolStripMenuItem";
            this.расчетToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.расчетToolStripMenuItem.Text = "Режим расчета";
            // 
            // TSMenuWoutCap
            // 
            this.TSMenuWoutCap.Checked = true;
            this.TSMenuWoutCap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TSMenuWoutCap.Name = "TSMenuWoutCap";
            this.TSMenuWoutCap.Size = new System.Drawing.Size(167, 22);
            this.TSMenuWoutCap.Text = "Плита";
            this.TSMenuWoutCap.Click += new System.EventHandler(this.TSMenuWoutCap_Click);
            // 
            // TSMenuWithCap
            // 
            this.TSMenuWithCap.Name = "TSMenuWithCap";
            this.TSMenuWithCap.Size = new System.Drawing.Size(167, 22);
            this.TSMenuWithCap.Text = "Плита+Капитель";
            this.TSMenuWithCap.Click += new System.EventHandler(this.TSMenuWithCap_Click);
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsAbout});
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.помощьToolStripMenuItem.Text = "Справка";
            // 
            // tsAbout
            // 
            this.tsAbout.Name = "tsAbout";
            this.tsAbout.Size = new System.Drawing.Size(149, 22);
            this.tsAbout.Text = "О программе";
            this.tsAbout.Click += new System.EventHandler(this.tsAbout_Click);
            // 
            // grBox_LocationColumn
            // 
            this.grBox_LocationColumn.Controls.Add(this.pBox_Corner);
            this.grBox_LocationColumn.Controls.Add(this.pBox_Center);
            this.grBox_LocationColumn.Controls.Add(this.pBox_Outside);
            this.grBox_LocationColumn.Location = new System.Drawing.Point(654, 57);
            this.grBox_LocationColumn.Name = "grBox_LocationColumn";
            this.grBox_LocationColumn.Size = new System.Drawing.Size(176, 391);
            this.grBox_LocationColumn.TabIndex = 1;
            this.grBox_LocationColumn.TabStop = false;
            this.grBox_LocationColumn.Text = "Выбор расположения  колонны относительно краев плиты";
            // 
            // pBox_Corner
            // 
            this.pBox_Corner.BackColor = System.Drawing.Color.White;
            this.pBox_Corner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBox_Corner.Image = ((System.Drawing.Image)(resources.GetObject("pBox_Corner.Image")));
            this.pBox_Corner.Location = new System.Drawing.Point(14, 269);
            this.pBox_Corner.Name = "pBox_Corner";
            this.pBox_Corner.Size = new System.Drawing.Size(148, 116);
            this.pBox_Corner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pBox_Corner.TabIndex = 3;
            this.pBox_Corner.TabStop = false;
            this.pBox_Corner.BackColorChanged += new System.EventHandler(this.tBox_TextChanged);
            this.pBox_Corner.Click += new System.EventHandler(this.pBox_Side_Click);
            // 
            // pBox_Center
            // 
            this.pBox_Center.BackColor = System.Drawing.Color.LightBlue;
            this.pBox_Center.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBox_Center.Image = ((System.Drawing.Image)(resources.GetObject("pBox_Center.Image")));
            this.pBox_Center.Location = new System.Drawing.Point(14, 41);
            this.pBox_Center.Name = "pBox_Center";
            this.pBox_Center.Size = new System.Drawing.Size(148, 112);
            this.pBox_Center.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pBox_Center.TabIndex = 2;
            this.pBox_Center.TabStop = false;
            this.pBox_Center.BackColorChanged += new System.EventHandler(this.tBox_TextChanged);
            this.pBox_Center.Click += new System.EventHandler(this.pBox_Side_Click);
            // 
            // pBox_Outside
            // 
            this.pBox_Outside.BackColor = System.Drawing.Color.White;
            this.pBox_Outside.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBox_Outside.Image = ((System.Drawing.Image)(resources.GetObject("pBox_Outside.Image")));
            this.pBox_Outside.Location = new System.Drawing.Point(14, 154);
            this.pBox_Outside.Name = "pBox_Outside";
            this.pBox_Outside.Size = new System.Drawing.Size(148, 114);
            this.pBox_Outside.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pBox_Outside.TabIndex = 0;
            this.pBox_Outside.TabStop = false;
            this.pBox_Outside.BackColorChanged += new System.EventHandler(this.tBox_TextChanged);
            this.pBox_Outside.Click += new System.EventHandler(this.pBox_Side_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(185, 247);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(281, 201);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            // 
            // grBoxConfig
            // 
            this.grBoxConfig.Controls.Add(this.label29);
            this.grBoxConfig.Controls.Add(this.label19);
            this.grBoxConfig.Controls.Add(this.label28);
            this.grBoxConfig.Controls.Add(this.label18);
            this.grBoxConfig.Controls.Add(this.label14);
            this.grBoxConfig.Controls.Add(this.tBoxMain_Asty);
            this.grBoxConfig.Controls.Add(this.label27);
            this.grBoxConfig.Controls.Add(this.tBoxMain_Astx);
            this.grBoxConfig.Controls.Add(this.label13);
            this.grBoxConfig.Controls.Add(this.label16);
            this.grBoxConfig.Controls.Add(this.tBox1_Ln);
            this.grBoxConfig.Controls.Add(this.label17);
            this.grBoxConfig.Controls.Add(this.tBox_Cy);
            this.grBoxConfig.Controls.Add(this.tBox_Cx);
            this.grBoxConfig.Controls.Add(this.label5);
            this.grBoxConfig.Controls.Add(this.label12);
            this.grBoxConfig.Controls.Add(this.label26);
            this.grBoxConfig.Controls.Add(this.label11);
            this.grBoxConfig.Controls.Add(this.label6);
            this.grBoxConfig.Controls.Add(this.btnCoverLayer);
            this.grBoxConfig.Controls.Add(this.label10);
            this.grBoxConfig.Controls.Add(this.label25);
            this.grBoxConfig.Controls.Add(this.label9);
            this.grBoxConfig.Controls.Add(this.tBox_acol);
            this.grBoxConfig.Controls.Add(this.tBox_Hcap);
            this.grBoxConfig.Controls.Add(this.label8);
            this.grBoxConfig.Controls.Add(this.label7);
            this.grBoxConfig.Controls.Add(this.label1);
            this.grBoxConfig.Controls.Add(this.tBox_Hpl);
            this.grBoxConfig.Controls.Add(this.label2);
            this.grBoxConfig.Controls.Add(this.tBox_bcol);
            this.grBoxConfig.Controls.Add(this.label4);
            this.grBoxConfig.Controls.Add(this.label3);
            this.grBoxConfig.Location = new System.Drawing.Point(12, 60);
            this.grBoxConfig.Name = "grBoxConfig";
            this.grBoxConfig.Size = new System.Drawing.Size(168, 388);
            this.grBoxConfig.TabIndex = 3;
            this.grBoxConfig.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 294);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(140, 13);
            this.label29.TabIndex = 109;
            this.label29.Text = "Продольное армирование";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(87, 336);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "см2";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 221);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(155, 13);
            this.label28.TabIndex = 108;
            this.label28.Text = "Расстояния до ц.т. арматуры";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(87, 314);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(27, 13);
            this.label18.TabIndex = 15;
            this.label18.Text = "см2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(110, 267);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "мм";
            // 
            // tBoxMain_Asty
            // 
            this.tBoxMain_Asty.Location = new System.Drawing.Point(46, 334);
            this.tBoxMain_Asty.Name = "tBoxMain_Asty";
            this.tBoxMain_Asty.ReadOnly = true;
            this.tBoxMain_Asty.Size = new System.Drawing.Size(37, 20);
            this.tBoxMain_Asty.TabIndex = 18;
            this.tBoxMain_Asty.Click += new System.EventHandler(this.tBoxMain_Ast_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 170);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 13);
            this.label27.TabIndex = 106;
            this.label27.Text = "Выступ капители";
            // 
            // tBoxMain_Astx
            // 
            this.tBoxMain_Astx.Location = new System.Drawing.Point(46, 310);
            this.tBoxMain_Astx.Name = "tBoxMain_Astx";
            this.tBoxMain_Astx.ReadOnly = true;
            this.tBoxMain_Astx.Size = new System.Drawing.Size(37, 20);
            this.tBoxMain_Astx.TabIndex = 108;
            this.tBoxMain_Astx.Tag = "n8";
            this.tBoxMain_Astx.Click += new System.EventHandler(this.tBoxMain_Ast_Click);
            this.tBoxMain_Astx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tBoxMain_Astx_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(110, 243);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "мм";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 336);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Ast,y=";
            // 
            // tBox1_Ln
            // 
            this.tBox1_Ln.Location = new System.Drawing.Point(42, 187);
            this.tBox1_Ln.Name = "tBox1_Ln";
            this.tBox1_Ln.Size = new System.Drawing.Size(64, 20);
            this.tBox1_Ln.TabIndex = 105;
            this.tBox1_Ln.Tag = "n5";
            this.tBox1_Ln.TextChanged += new System.EventHandler(this.tBox_TextChanged);
            this.tBox1_Ln.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_acol_KeyDown);
            this.tBox1_Ln.Leave += new System.EventHandler(this.tBox_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 313);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "Ast,x=";
            // 
            // tBox_Cy
            // 
            this.tBox_Cy.Location = new System.Drawing.Point(42, 264);
            this.tBox_Cy.Name = "tBox_Cy";
            this.tBox_Cy.Size = new System.Drawing.Size(64, 20);
            this.tBox_Cy.TabIndex = 107;
            this.tBox_Cy.Tag = "n7";
            this.tBox_Cy.TextChanged += new System.EventHandler(this.tBox_TextChanged);
            this.tBox_Cy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_acol_KeyDown);
            this.tBox_Cy.Leave += new System.EventHandler(this.tBox_Leave);
            // 
            // tBox_Cx
            // 
            this.tBox_Cx.Location = new System.Drawing.Point(42, 238);
            this.tBox_Cx.Name = "tBox_Cx";
            this.tBox_Cx.Size = new System.Drawing.Size(64, 20);
            this.tBox_Cx.TabIndex = 106;
            this.tBox_Cx.Tag = "n6";
            this.tBox_Cx.TextChanged += new System.EventHandler(this.tBox_TextChanged);
            this.tBox_Cx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_acol_KeyDown);
            this.tBox_Cx.Leave += new System.EventHandler(this.tBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Ln=";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(25, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Cy=";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 96);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(146, 13);
            this.label26.TabIndex = 105;
            this.label26.Text = "Толщина плиты и капители";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 242);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Cx=";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(112, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "мм";
            // 
            // btnCoverLayer
            // 
            this.btnCoverLayer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCoverLayer.BackgroundImage")));
            this.btnCoverLayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCoverLayer.Location = new System.Drawing.Point(133, 247);
            this.btnCoverLayer.Name = "btnCoverLayer";
            this.btnCoverLayer.Size = new System.Drawing.Size(30, 30);
            this.btnCoverLayer.TabIndex = 9;
            this.btnCoverLayer.UseVisualStyleBackColor = true;
            this.btnCoverLayer.Click += new System.EventHandler(this.btnCoverLayer_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 139);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Hk=";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(101, 13);
            this.label25.TabIndex = 103;
            this.label25.Text = "Размеры колонны";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(112, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "мм";
            // 
            // tBox_acol
            // 
            this.tBox_acol.Location = new System.Drawing.Point(42, 37);
            this.tBox_acol.Name = "tBox_acol";
            this.tBox_acol.Size = new System.Drawing.Size(64, 20);
            this.tBox_acol.TabIndex = 101;
            this.tBox_acol.Tag = "n1";
            this.tBox_acol.TextChanged += new System.EventHandler(this.tBox_TextChanged);
            this.tBox_acol.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_acol_KeyDown);
            this.tBox_acol.Leave += new System.EventHandler(this.tBox_Leave);
            // 
            // tBox_Hcap
            // 
            this.tBox_Hcap.Location = new System.Drawing.Point(42, 138);
            this.tBox_Hcap.Name = "tBox_Hcap";
            this.tBox_Hcap.Size = new System.Drawing.Size(64, 20);
            this.tBox_Hcap.TabIndex = 104;
            this.tBox_Hcap.Tag = "n4";
            this.tBox_Hcap.TextChanged += new System.EventHandler(this.tBox_TextChanged);
            this.tBox_Hcap.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_acol_KeyDown);
            this.tBox_Hcap.Leave += new System.EventHandler(this.tBox_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "H=";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(112, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "мм";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "acol=";
            // 
            // tBox_Hpl
            // 
            this.tBox_Hpl.Location = new System.Drawing.Point(42, 112);
            this.tBox_Hpl.Name = "tBox_Hpl";
            this.tBox_Hpl.Size = new System.Drawing.Size(64, 20);
            this.tBox_Hpl.TabIndex = 103;
            this.tBox_Hpl.Tag = "n3";
            this.tBox_Hpl.TextChanged += new System.EventHandler(this.tBox_TextChanged);
            this.tBox_Hpl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_acol_KeyDown);
            this.tBox_Hpl.Leave += new System.EventHandler(this.tBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "bcol=";
            // 
            // tBox_bcol
            // 
            this.tBox_bcol.Location = new System.Drawing.Point(42, 63);
            this.tBox_bcol.Name = "tBox_bcol";
            this.tBox_bcol.Size = new System.Drawing.Size(64, 20);
            this.tBox_bcol.TabIndex = 102;
            this.tBox_bcol.Tag = "n2";
            this.tBox_bcol.TextChanged += new System.EventHandler(this.tBox_TextChanged);
            this.tBox_bcol.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_acol_KeyDown);
            this.tBox_bcol.Leave += new System.EventHandler(this.tBox_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(112, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "мм";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(112, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "мм";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(185, 69);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(281, 179);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(246, 56);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(177, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Общий вид расчетных элементов";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cmBox_Diam_St);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.cmBox_ClassSteel);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.cmBox_classConcrete);
            this.groupBox8.Location = new System.Drawing.Point(473, 57);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(176, 118);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Характеристики материалов";
            // 
            // cmBox_Diam_St
            // 
            this.cmBox_Diam_St.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBox_Diam_St.FormattingEnabled = true;
            this.cmBox_Diam_St.Items.AddRange(new object[] {
            "6",
            "8",
            "10",
            "12",
            "14"});
            this.cmBox_Diam_St.Location = new System.Drawing.Point(105, 91);
            this.cmBox_Diam_St.Name = "cmBox_Diam_St";
            this.cmBox_Diam_St.Size = new System.Drawing.Size(66, 21);
            this.cmBox_Diam_St.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 93);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Диам.  Asw:";
            // 
            // cmBox_ClassSteel
            // 
            this.cmBox_ClassSteel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBox_ClassSteel.FormattingEnabled = true;
            this.cmBox_ClassSteel.Items.AddRange(new object[] {
            "S500",
            "S240"});
            this.cmBox_ClassSteel.Location = new System.Drawing.Point(105, 62);
            this.cmBox_ClassSteel.Name = "cmBox_ClassSteel";
            this.cmBox_ClassSteel.Size = new System.Drawing.Size(66, 21);
            this.cmBox_ClassSteel.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Класс  Asw:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 34);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(82, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Класс  бетона:";
            // 
            // cmBox_classConcrete
            // 
            this.cmBox_classConcrete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBox_classConcrete.FormattingEnabled = true;
            this.cmBox_classConcrete.Items.AddRange(new object[] {
            "С 16/20",
            "С 20/25",
            "С 25/30",
            "С 30/37",
            "С 35/45",
            "С 40/50"});
            this.cmBox_classConcrete.Location = new System.Drawing.Point(105, 31);
            this.cmBox_classConcrete.Name = "cmBox_classConcrete";
            this.cmBox_classConcrete.Size = new System.Drawing.Size(66, 21);
            this.cmBox_classConcrete.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.tBoxLoad);
            this.groupBox9.Location = new System.Drawing.Point(473, 181);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(176, 50);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Расчетная нагрузка";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(82, 26);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "тонн";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 26);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(20, 13);
            this.label23.TabIndex = 5;
            this.label23.Text = "P=";
            // 
            // tBoxLoad
            // 
            this.tBoxLoad.Location = new System.Drawing.Point(29, 23);
            this.tBoxLoad.Name = "tBoxLoad";
            this.tBoxLoad.Size = new System.Drawing.Size(50, 20);
            this.tBoxLoad.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlStrStatL,
            this.tlSStatusLMode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 451);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(837, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tlStrStatL
            // 
            this.tlStrStatL.BackColor = System.Drawing.SystemColors.Control;
            this.tlStrStatL.Name = "tlStrStatL";
            this.tlStrStatL.Size = new System.Drawing.Size(10, 17);
            this.tlStrStatL.Text = " ";
            // 
            // tlSStatusLMode
            // 
            this.tlSStatusLMode.Name = "tlSStatusLMode";
            this.tlSStatusLMode.Size = new System.Drawing.Size(169, 17);
            this.tlSStatusLMode.Text = "Режим расчета: без капители";
            // 
            // btnCalcSol
            // 
            this.btnCalcSol.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnCalcSol.Location = new System.Drawing.Point(6, 20);
            this.btnCalcSol.Name = "btnCalcSol";
            this.btnCalcSol.Size = new System.Drawing.Size(80, 30);
            this.btnCalcSol.TabIndex = 9;
            this.btnCalcSol.Text = "Расчитать";
            this.btnCalcSol.UseVisualStyleBackColor = false;
            this.btnCalcSol.Click += new System.EventHandler(this.btnCalcSol_Click);
            // 
            // btnAutoSol
            // 
            this.btnAutoSol.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnAutoSol.Location = new System.Drawing.Point(91, 20);
            this.btnAutoSol.Name = "btnAutoSol";
            this.btnAutoSol.Size = new System.Drawing.Size(80, 30);
            this.btnAutoSol.TabIndex = 10;
            this.btnAutoSol.Text = "Подобрать";
            this.btnAutoSol.UseVisualStyleBackColor = false;
            this.btnAutoSol.Click += new System.EventHandler(this.btnAutoSol_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.chbUseAsw);
            this.groupBox10.Controls.Add(this.btnCalcSol);
            this.groupBox10.Controls.Add(this.btnAutoSol);
            this.groupBox10.Location = new System.Drawing.Point(473, 370);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(176, 78);
            this.groupBox10.TabIndex = 11;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Расчет";
            // 
            // chbUseAsw
            // 
            this.chbUseAsw.AutoSize = true;
            this.chbUseAsw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chbUseAsw.Location = new System.Drawing.Point(6, 57);
            this.chbUseAsw.Name = "chbUseAsw";
            this.chbUseAsw.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbUseAsw.Size = new System.Drawing.Size(103, 17);
            this.chbUseAsw.TabIndex = 10;
            this.chbUseAsw.Text = "установить Asw";
            this.chbUseAsw.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.AliceBlue;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(25, 25);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnOpenFile,
            this.tsBtnQickSave,
            this.tsBtnSaveAs,
            this.tsBtnExit,
            this.toolStripSeparator1,
            this.tsBtnMode});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStrip1.Size = new System.Drawing.Size(837, 32);
            this.toolStrip1.TabIndex = 12;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsBtnOpenFile
            // 
            this.tsBtnOpenFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnOpenFile.Image")));
            this.tsBtnOpenFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnOpenFile.Name = "tsBtnOpenFile";
            this.tsBtnOpenFile.Size = new System.Drawing.Size(29, 29);
            this.tsBtnOpenFile.Text = "Открыть расчет...";
            this.tsBtnOpenFile.Click += new System.EventHandler(this.tsOpenFile_Click);
            // 
            // tsBtnQickSave
            // 
            this.tsBtnQickSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnQickSave.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnQickSave.Image")));
            this.tsBtnQickSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnQickSave.Name = "tsBtnQickSave";
            this.tsBtnQickSave.Size = new System.Drawing.Size(29, 29);
            this.tsBtnQickSave.Text = "Сохранить...";
            this.tsBtnQickSave.Click += new System.EventHandler(this.tsSave_Click);
            // 
            // tsBtnSaveAs
            // 
            this.tsBtnSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnSaveAs.Image")));
            this.tsBtnSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSaveAs.Name = "tsBtnSaveAs";
            this.tsBtnSaveAs.Size = new System.Drawing.Size(29, 29);
            this.tsBtnSaveAs.Text = "Сохранить как...";
            this.tsBtnSaveAs.Click += new System.EventHandler(this.tsmSaveData_Click);
            // 
            // tsBtnExit
            // 
            this.tsBtnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnExit.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnExit.Image")));
            this.tsBtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnExit.Name = "tsBtnExit";
            this.tsBtnExit.Size = new System.Drawing.Size(29, 29);
            this.tsBtnExit.Text = "Выйти";
            this.tsBtnExit.Click += new System.EventHandler(this.tsExit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 32);
            // 
            // tsBtnMode
            // 
            this.tsBtnMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnModeSlab,
            this.tsBtnModeCap});
            this.tsBtnMode.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnMode.Image")));
            this.tsBtnMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnMode.Name = "tsBtnMode";
            this.tsBtnMode.Size = new System.Drawing.Size(41, 29);
            this.tsBtnMode.Text = "Режим расчета";
            // 
            // tsBtnModeSlab
            // 
            this.tsBtnModeSlab.Name = "tsBtnModeSlab";
            this.tsBtnModeSlab.Size = new System.Drawing.Size(173, 22);
            this.tsBtnModeSlab.Text = "Плита";
            this.tsBtnModeSlab.Click += new System.EventHandler(this.TSMenuWoutCap_Click);
            // 
            // tsBtnModeCap
            // 
            this.tsBtnModeCap.Name = "tsBtnModeCap";
            this.tsBtnModeCap.Size = new System.Drawing.Size(173, 22);
            this.tsBtnModeCap.Text = "Плита + Капитель";
            this.tsBtnModeCap.Click += new System.EventHandler(this.TSMenuWithCap_Click);
            // 
            // rTextBoxComments
            // 
            this.rTextBoxComments.BackColor = System.Drawing.SystemColors.Info;
            this.rTextBoxComments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rTextBoxComments.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rTextBoxComments.ForeColor = System.Drawing.SystemColors.Highlight;
            this.rTextBoxComments.Location = new System.Drawing.Point(473, 238);
            this.rTextBoxComments.Name = "rTextBoxComments";
            this.rTextBoxComments.Size = new System.Drawing.Size(175, 126);
            this.rTextBoxComments.TabIndex = 13;
            this.rTextBoxComments.Text = "Введите комментарий к расчету...\n";
            this.rTextBoxComments.MouseLeave += new System.EventHandler(this.rTextBoxComments_MouseLeave);
            this.rTextBoxComments.MouseMove += new System.Windows.Forms.MouseEventHandler(this.rTextBoxComments_MouseMove);
            // 
            // FrmStartWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 473);
            this.Controls.Add(this.rTextBoxComments);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.grBoxConfig);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.grBox_LocationColumn);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FrmStartWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Расчет на продавливание ж.б. плит по СНБ 5.03.01-02 (ver. 1.0.0)";
            this.Load += new System.EventHandler(this.TSMenuWoutCap_Click);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grBox_LocationColumn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Corner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Center)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Outside)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.grBoxConfig.ResumeLayout(false);
            this.grBoxConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsOpenFile;
        private System.Windows.Forms.ToolStripMenuItem tsmSaveAs;
        private System.Windows.Forms.ToolStripMenuItem tsExit;
        private System.Windows.Forms.ToolStripMenuItem расчетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        private System.Windows.Forms.GroupBox grBox_LocationColumn;
        private System.Windows.Forms.PictureBox pBox_Outside;
        private System.Windows.Forms.PictureBox pBox_Corner;
        private System.Windows.Forms.PictureBox pBox_Center;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox grBoxConfig;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tBox_bcol;
        private System.Windows.Forms.TextBox tBox_acol;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCoverLayer;
        private System.Windows.Forms.TextBox tBox1_Ln;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tBox_Hcap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tBox_Hpl;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tBox_Cy;
        private System.Windows.Forms.TextBox tBox_Cx;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox tBoxMain_Asty;
        public System.Windows.Forms.TextBox tBoxMain_Astx;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox cmBox_ClassSteel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmBox_Diam_St;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmBox_classConcrete;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tBoxLoad;
        public System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel tlStrStatL;
        private System.Windows.Forms.ToolStripMenuItem TSMenuWoutCap;
        private System.Windows.Forms.ToolStripMenuItem TSMenuWithCap;
        private System.Windows.Forms.ToolStripStatusLabel tlSStatusLMode;
        private System.Windows.Forms.Button btnCalcSol;
        private System.Windows.Forms.Button btnAutoSol;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox chbUseAsw;
        private System.Windows.Forms.ToolStripMenuItem tsSave;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsBtnOpenFile;
        private System.Windows.Forms.ToolStripButton tsBtnQickSave;
        private System.Windows.Forms.ToolStripButton tsBtnSaveAs;
        private System.Windows.Forms.ToolStripButton tsBtnExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSplitButton tsBtnMode;
        private System.Windows.Forms.ToolStripMenuItem tsBtnModeSlab;
        private System.Windows.Forms.ToolStripMenuItem tsBtnModeCap;
        private System.Windows.Forms.ToolStripMenuItem tsAbout;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.RichTextBox rTextBoxComments;
    }
}

