﻿namespace Application_for_Calc_Punch_Building_
{
    partial class FrmReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReport));
            this.dataGV_Report = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.rTB_report = new System.Windows.Forms.RichTextBox();
            this.btnShowAsw = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.grBoxSizeElements = new System.Windows.Forms.GroupBox();
            this.lbHcap = new System.Windows.Forms.Label();
            this.lbLcap = new System.Windows.Forms.Label();
            this.lbHpl = new System.Windows.Forms.Label();
            this.lbBcol = new System.Windows.Forms.Label();
            this.lbAcol = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnHideAsw = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGV_Report)).BeginInit();
            this.grBoxSizeElements.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGV_Report
            // 
            this.dataGV_Report.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGV_Report.Location = new System.Drawing.Point(4, 26);
            this.dataGV_Report.Name = "dataGV_Report";
            this.dataGV_Report.Size = new System.Drawing.Size(475, 130);
            this.dataGV_Report.TabIndex = 0;
            this.dataGV_Report.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGV_Report_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(248, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Таблица расчетных параметров плиты без Asw";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(4, 243);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(77, 33);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ок";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rTB_report
            // 
            this.rTB_report.Location = new System.Drawing.Point(4, 162);
            this.rTB_report.Name = "rTB_report";
            this.rTB_report.ReadOnly = true;
            this.rTB_report.Size = new System.Drawing.Size(270, 75);
            this.rTB_report.TabIndex = 3;
            this.rTB_report.Text = "";
            // 
            // btnShowAsw
            // 
            this.btnShowAsw.Location = new System.Drawing.Point(87, 243);
            this.btnShowAsw.Name = "btnShowAsw";
            this.btnShowAsw.Size = new System.Drawing.Size(93, 33);
            this.btnShowAsw.TabIndex = 4;
            this.btnShowAsw.Text = "Показать Asw";
            this.btnShowAsw.UseVisualStyleBackColor = true;
            this.btnShowAsw.Visible = false;
            this.btnShowAsw.Click += new System.EventHandler(this.btnShowAsw_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Acol=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Bcol=";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(94, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Lкап.=";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Hпл.";
            // 
            // grBoxSizeElements
            // 
            this.grBoxSizeElements.Controls.Add(this.lbHcap);
            this.grBoxSizeElements.Controls.Add(this.lbLcap);
            this.grBoxSizeElements.Controls.Add(this.lbHpl);
            this.grBoxSizeElements.Controls.Add(this.lbBcol);
            this.grBoxSizeElements.Controls.Add(this.lbAcol);
            this.grBoxSizeElements.Controls.Add(this.label6);
            this.grBoxSizeElements.Controls.Add(this.label4);
            this.grBoxSizeElements.Controls.Add(this.label5);
            this.grBoxSizeElements.Controls.Add(this.label2);
            this.grBoxSizeElements.Controls.Add(this.label3);
            this.grBoxSizeElements.Location = new System.Drawing.Point(280, 162);
            this.grBoxSizeElements.Name = "grBoxSizeElements";
            this.grBoxSizeElements.Size = new System.Drawing.Size(199, 75);
            this.grBoxSizeElements.TabIndex = 9;
            this.grBoxSizeElements.TabStop = false;
            this.grBoxSizeElements.Text = "Расчетные размеры";
            // 
            // lbHcap
            // 
            this.lbHcap.AutoSize = true;
            this.lbHcap.Location = new System.Drawing.Point(140, 33);
            this.lbHcap.Name = "lbHcap";
            this.lbHcap.Size = new System.Drawing.Size(41, 13);
            this.lbHcap.TabIndex = 14;
            this.lbHcap.Text = "lbHcap";
            // 
            // lbLcap
            // 
            this.lbLcap.AutoSize = true;
            this.lbLcap.Location = new System.Drawing.Point(140, 16);
            this.lbLcap.Name = "lbLcap";
            this.lbLcap.Size = new System.Drawing.Size(39, 13);
            this.lbLcap.TabIndex = 13;
            this.lbLcap.Text = "lbLcap";
            // 
            // lbHpl
            // 
            this.lbHpl.AutoSize = true;
            this.lbHpl.Location = new System.Drawing.Point(46, 51);
            this.lbHpl.Name = "lbHpl";
            this.lbHpl.Size = new System.Drawing.Size(31, 13);
            this.lbHpl.TabIndex = 12;
            this.lbHpl.Text = "lbHpl";
            // 
            // lbBcol
            // 
            this.lbBcol.AutoSize = true;
            this.lbBcol.Location = new System.Drawing.Point(46, 33);
            this.lbBcol.Name = "lbBcol";
            this.lbBcol.Size = new System.Drawing.Size(36, 13);
            this.lbBcol.TabIndex = 11;
            this.lbBcol.Text = "lbBcol";
            // 
            // lbAcol
            // 
            this.lbAcol.AutoSize = true;
            this.lbAcol.Location = new System.Drawing.Point(46, 16);
            this.lbAcol.Name = "lbAcol";
            this.lbAcol.Size = new System.Drawing.Size(36, 13);
            this.lbAcol.TabIndex = 10;
            this.lbAcol.Text = "lbAcol";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(94, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Hкап.=";
            // 
            // btnHideAsw
            // 
            this.btnHideAsw.Location = new System.Drawing.Point(186, 243);
            this.btnHideAsw.Name = "btnHideAsw";
            this.btnHideAsw.Size = new System.Drawing.Size(88, 33);
            this.btnHideAsw.TabIndex = 10;
            this.btnHideAsw.Text = "Скрыть Asw";
            this.btnHideAsw.UseVisualStyleBackColor = true;
            this.btnHideAsw.Click += new System.EventHandler(this.btnHideAsw_Click);
            // 
            // FrmReport
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(482, 283);
            this.Controls.Add(this.btnHideAsw);
            this.Controls.Add(this.grBoxSizeElements);
            this.Controls.Add(this.btnShowAsw);
            this.Controls.Add(this.rTB_report);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGV_Report);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отчет расчета плиты на продавливание";
            this.Load += new System.EventHandler(this.FrmReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGV_Report)).EndInit();
            this.grBoxSizeElements.ResumeLayout(false);
            this.grBoxSizeElements.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGV_Report;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.RichTextBox rTB_report;
        private System.Windows.Forms.Button btnShowAsw;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox grBoxSizeElements;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbHcap;
        private System.Windows.Forms.Label lbLcap;
        private System.Windows.Forms.Label lbHpl;
        private System.Windows.Forms.Label lbBcol;
        private System.Windows.Forms.Label lbAcol;
        private System.Windows.Forms.Button btnHideAsw;
    }
}