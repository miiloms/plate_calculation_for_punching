﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Application_for_Calc_Punch_Building_
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FrmStartWindow());

        }
    }
    static class  ContentDataSlab //статический класс для передачи информации между формами
    {
        public static double _thicknessSlab{get;set;}
        public static double _thicknessCap { get; set; } //толщина капители мм++
        public static Size _sizeCol { get; set; } //размер колонны в мм++
        public static Size _capCol { get; set; }//размер капители в мм++
        public static Point _coverConcrete { get; set; } //расстояние до ц.т. арматуры в мм++
        public static  double _astX { get; set; } // армирование в см2 на ширине плиты по X++
        public static  double _astY { get; set; } // армирование в см2 на ширине плиты по Y++
        public static  uint _valueFck { get; set; }// текущее значение нормативной прочности бетона++
        public static double _valueSqwSt { get; set; } //значение площади стержней арматуры в см2++
        public static double _valueStrngSt { get; set; } //значение нормативной прочности арматуры++
        public static  position _posColumn { get; set; } //расположение колонны относительно краев плиты++
        public static double _load { get; set; } // нагрузка расчетная на колонну в Тоннах++
        /// <summary>
        /// 
        /// </summary>
        public static double _wHeight { get; set; }// рабочая высота расчетного сечения
        public static double _PL { get; set; }// коэфф
        public static double _VRdc_ex { get; set; }
        public static double _VRdc_in { get; set; }
        public static double _Ucrit_in { get; set; }
        public static double _Ucrit_ex { get; set; }
        public static double _Vsd_in { get; set; }
        public static double _Vsd_ex { get; set; }
        public static double _reservStrong { get; set; }
    }
    enum position { center, corner, outside }; // расположение колонны
    static class ClassConcrete   //классы бетона и его нормативные значения н/мм2 (МПа)
    {
         public const uint C16_20 = 16, C20_25 = 20, C25_30 = 25, C30_37 = 30, C35_45 = 35, C40_50 = 40;
         public const double C16_20_ctm = 1.9, C20_25_ctm = 2.2, C25_30_ctm = 2.6, C30_37_ctm = 2.9, C35_45_ctm = 3.2, C40_50_ctm = 3.5;
         public const double C16_20_ctk = 1.3, C20_25_ctk = 1.5, C25_30_ctk = 1.8, C30_37_ctk = 2, C35_45_ctk = 2.2, C40_50_ctk = 2.5;

    }
    static class SqwSteel  //площади сечения арматурных стержней  см2
    {
        public const double D6 = 0.283, D8 = 0.503, D10 = 0.785, D12 = 1.131, D14 = 1.539;
    }
    static class ClassSteel
    {
        public const double S500 = 348, S240 = 174;
    }
  public   class PerimetrSteel
    {
      public uint _numb_elements;
        public double _length_per;
        public double _Vrd_sy;
        public double _Vsd;
        public double _Swtot;
       public  PerimetrSteel(uint numb_elem, double length_per, double Vrd_sy, double Vsd, double SwTot )
        {
            this._numb_elements = numb_elem;
            this._length_per = Math.Round(length_per, 0);
            this._Vrd_sy = Math.Round(Vrd_sy, 2);
            this._Vsd = Math.Round(Vsd, 2);
            this._Swtot = SwTot;
        }
    }
    public class Slab
    {
        double thicknessSlab; //толщина плиты мм++
        double thicknessCap; //толщина капители мм++
        Size sizeCol; //размер колонны в мм++
        Size capCol;//размер капители в мм++
        Point coverConcrete; //расстояние до ц.т. арматуры в мм++
        double astX; // армирование в см2 на ширине плиты по X++
        double astY; // армирование в см2 на ширине плиты по Y++
        uint valueFck;// текущее значение нормативной прочности бетона++
        double valueSqwSt; //значение площади стержней арматуры в см2++
        double valueStrngSt; //значение нормативной прочности арматуры++
        position posColumn; //расположение колонны относительно краев плиты++
        double load; // нагрузка расчетная на колонну в Тоннах++
        double perStr;
        /////////////////////////////////////////////////////////////////////
        double Dcrit_ex; //Dcrit,ex критический наружный диаметр(от края колонны) при Ln>1.5*(d+hn) или при Ln<1.5*hn
        double Dcrit_in; //Dcrit,in критический внутренний диаметр(от края колонны) при Ln>1.5*(d+hn)
        double Ucrit_in;//крит внутренний периметр с капителью
        double Ucrit_ex; //крит наружный периметр с капительью или без
        double Vsd_in;
        double Vsd_ex;
        double U0;
        double Fctm;
        double Fctk;
        List<PerimetrSteel> slab1_cont;
        public Slab()
        {
            this.thicknessSlab = 0;
            this.thicknessCap = 0;
            this.sizeCol = new Size(0, 0);
            this.capCol = new Size(0, 0);
            this.astX = 0;
            this.astY = 0;
            this.posColumn = position.center;
            this.load = 0;
            this.valueFck = ClassConcrete.C16_20;
            this.Fctm = ClassConcrete.C16_20_ctm;
            this.Fctk = ClassConcrete.C16_20_ctk;
            this.valueSqwSt = SqwSteel.D6;
            this.valueStrngSt = ClassSteel.S240;
            this.Dcrit_ex = 0;
            this.Dcrit_in = 0;
            this.Ucrit_in = 0;
            this.Ucrit_ex = 0;
            this.Vsd_ex = 0;
            this.Vsd_in = 0;
            this.U0 = 0;
        }
        public void ReciveDataSlab() //прием данных для расчета (данные обновляються из  статического класса ContentDataSlab)
        {

            this.thicknessSlab = ContentDataSlab._thicknessSlab;
            this.thicknessCap = ContentDataSlab._thicknessCap;
            this.sizeCol = ContentDataSlab._sizeCol;
            this.capCol = ContentDataSlab._capCol;
            this.coverConcrete = ContentDataSlab._coverConcrete;
            this.astX = ContentDataSlab._astX;
            this.astY = ContentDataSlab._astY;
            this.posColumn = ContentDataSlab._posColumn;
            this.load = ContentDataSlab._load;
            this.valueFck = ContentDataSlab._valueFck;
            this.valueSqwSt = ContentDataSlab._valueSqwSt;
            this.valueStrngSt = ContentDataSlab._valueStrngSt;
            this.Dcrit_ex = 0;
            this.Dcrit_in = 0;
            ContentDataSlab._Ucrit_ex =this.Ucrit_ex = 0;
            ContentDataSlab._Ucrit_in = this.Ucrit_in = 0;
            ContentDataSlab._Vsd_ex= this.Vsd_ex = 0;
            ContentDataSlab._Vsd_in = this.Vsd_in = 0;
            perStr = ContentDataSlab._reservStrong;
            switch (ContentDataSlab._valueFck)
            {
                case ClassConcrete.C16_20: this.Fctm = ClassConcrete.C16_20_ctm; break;
                case ClassConcrete.C20_25: this.Fctm = ClassConcrete.C20_25_ctm; break;
                case ClassConcrete.C25_30: this.Fctm = ClassConcrete.C25_30_ctm; break;
                case ClassConcrete.C30_37: this.Fctm = ClassConcrete.C30_37_ctm; break;
                case ClassConcrete.C35_45: this.Fctm = ClassConcrete.C35_45_ctm; break;
                case ClassConcrete.C40_50: this.Fctm = ClassConcrete.C40_50_ctm; break;
            }
            switch (ContentDataSlab._valueFck)
            {
                case ClassConcrete.C16_20: this.Fctk = ClassConcrete.C16_20_ctk; break;
                case ClassConcrete.C20_25: this.Fctk = ClassConcrete.C20_25_ctk; break;
                case ClassConcrete.C25_30: this.Fctk = ClassConcrete.C25_30_ctk; break;
                case ClassConcrete.C30_37: this.Fctk = ClassConcrete.C30_37_ctk; break;
                case ClassConcrete.C35_45: this.Fctk = ClassConcrete.C35_45_ctk; break;
                case ClassConcrete.C40_50: this.Fctk = ClassConcrete.C40_50_ctk; break;
            }

        }
        public void SendDataSlab(double H)
        {
            ContentDataSlab._wHeight = this.WorkHeight;
            ContentDataSlab._Ucrit_in =  Math.Round(this.Ucrit_in,0);
            ContentDataSlab._Ucrit_ex =  Math.Round(this.Ucrit_ex,0);
            ContentDataSlab._Vsd_in =  Math.Round(this.Vsd_in,2);
            ContentDataSlab._Vsd_ex =  Math.Round(this.Vsd_ex,2);
            ContentDataSlab._PL = Math.Round(this.PL(this.WorkHeight), 6);
            if (this.Dcrit_in != 0)
            {
                ContentDataSlab._VRdc_ex = Math.Round(this.VRdc(this.WorkHeight - thicknessCap), 2);
                ContentDataSlab._VRdc_in = Math.Round(this.VRdc(this.WorkHeight), 2);
            }
            else
            {
                ContentDataSlab._VRdc_ex = Math.Round(this.VRdc(this.WorkHeight- thicknessCap), 2);
                ContentDataSlab._VRdc_in = 0;
            }
        }
        public double WorkHeight  //рабочая высота
        {
            get {return thicknessCap + thicknessSlab -(coverConcrete.X+ coverConcrete.Y)* 0.5;}
        }
        double K(double wHeight) //коэфф К
        {
            double tempK = 1 + Math.Sqrt(200 / wHeight);
                return tempK>2?2:tempK;  
        }

       public  double WidthX1(double wHeight)  //ширина полосы плиты для учета  коэф. прод. армирования вдоль X в мм
        {
                switch(posColumn)
                {
                    case position.center: return sizeCol.Width + 3 * wHeight;
                    case position.outside: return sizeCol.Width + 3 * wHeight;
                    case position.corner: return sizeCol.Width + 1.5 * wHeight;
                    default: return 0;
                } 
        }

       public  double WidthY1(double wHeight) //ширина полосы плиты для учета  коэф. прод. армирования вдоль Y в мм
        {
                switch (posColumn)
                {
                    case position.center: return sizeCol.Height + 3 * wHeight;
                    case position.outside: return sizeCol.Height + 1.5 * wHeight;
                    case position.corner: return sizeCol.Width + 1.5 * wHeight;
                    default: return 0;
                }
        }
       double PL(double wHeight) //коэфф продольного армирования
        {
            double _tWH = this.WorkHeight;
            double _tWX = this.WidthX1(_tWH);
            double _tWY = this.WidthY1(_tWH);
            double _astX = (astX / _tWX) * this.WidthX1(wHeight); // интерполяция площади продольного армирования вдоль X для капители и без 
            double _astY = (astY / _tWY) * this.WidthY1(wHeight);  // интерполяция площади продольного армирования вдоль Y для капители и без 
           var temp = Math.Sqrt((_astX * 100 / (WidthX1(wHeight) * wHeight)) * (_astY * 100 / (WidthY1(wHeight) * wHeight)));
            return temp;
        }

        double VRdc(double wHeight) // погонная прочность сечения в Т/м
        {
            var temp = Math.Max((0.15 * this.K(wHeight) * Math.Pow((100 * this.PL(wHeight) * this.valueFck), (double)1.0 / 3.0) * wHeight * 0.1), (0.5 * (this.Fctk / 1.5) * wHeight) * 0.1);
            return temp;
        }
        void  calcDCrits() // вычислить критические диаметры(от края колонны до края крит линии)  для плит с капителью  Dcrit/Dcrit,ex/Dcrit,in
        {
            
            double tLn=capCol.Width*0.5-sizeCol.Width*0.5; //расстояние от края колонны до края капители
            //далее считаем если есть капитель
            if(tLn<=1.5*this.thicknessCap)// при Ln<1.5*hn
            {
                double tDcrit1, tDcrit2;
                tDcrit1 = 1.5 * (WorkHeight-this.thicknessCap) + 0.56 * Math.Sqrt(capCol.Width*capCol.Height)-(sizeCol.Width+sizeCol.Height)*0.5;
                tDcrit2 = 1.5 * (WorkHeight - this.thicknessCap) + 0.69 * Math.Min(capCol.Height, capCol.Width) - Math.Min(sizeCol.Width, sizeCol.Height);
                this.Dcrit_ex = Math.Min(tDcrit1, tDcrit2);
                this.Dcrit_in = 0;
                return;
            }
            if (tLn > 1.5 * (WorkHeight))// при Ln>1.5*(d+hn)
            {
                this.Dcrit_ex = tLn + 1.5 * (WorkHeight - this.thicknessCap);
                this.Dcrit_in = 1.5 * (WorkHeight);
                return;
            }
            if (1.5 * this.thicknessCap < tLn && tLn < 1.5 * (WorkHeight))//при 1,5hn<ln<1.5(hn+d)
            {
                Dcrit_ex = 1.5 * tLn;
                this.Dcrit_in = 0;
                return;
            }

        }
        void calcUcrits() //вычислить критические периметры c капителью и без
        {
            
            if (this.thicknessCap == 0)
            {
                switch (posColumn)
                {
                    case position.center: this.Ucrit_ex = sizeCol.Height * 2 + sizeCol.Width * 2 + 1.5 * WorkHeight * Math.PI * 2; break;
                    case position.outside: this.Ucrit_ex = sizeCol.Height * 2 + sizeCol.Width + 1.5 * WorkHeight * Math.PI; break;
                    default: this.Ucrit_ex = sizeCol.Height + sizeCol.Width + 1.5 * WorkHeight * Math.PI * 0.5; break;
                }
                return;
            }
            //для расчет критических периметров при наличие капители
            this.calcDCrits();
            double tLn = capCol.Width * 0.5 - sizeCol.Width * 0.5; //расстояние от края колонны до края капители
            double tRcr; // для радиуса поворота крит периметра на углах капители
            if (this.Dcrit_in == 0)
            {
                tRcr = this.Dcrit_ex - (capCol.Width * 0.5 - sizeCol.Width * 0.5);
                switch (posColumn)
                {
                    case position.center: this.Ucrit_ex = (sizeCol.Height + tLn) * 2 + (sizeCol.Width + tLn) * 2 + tRcr * Math.PI * 2; break;
                    case position.outside: this.Ucrit_ex = (sizeCol.Height + tLn) * 2 + sizeCol.Width + tLn + tRcr * Math.PI; break;
                    default: this.Ucrit_ex = sizeCol.Height + sizeCol.Width + tLn * 2 + tRcr * Math.PI * 0.5; break;
                }
                return;
            }
            if (this.Dcrit_in != 0)
            {
                tRcr = this.Dcrit_ex - (capCol.Width * 0.5 - sizeCol.Width * 0.5);//радиус скругления углов  при наружном критическом периметре
                switch (posColumn)
                {
                    case position.center: this.Ucrit_ex = (sizeCol.Height + tLn) * 2 + (sizeCol.Width + tLn) * 2 + tRcr * Math.PI * 2; break;
                    case position.outside: this.Ucrit_ex = (sizeCol.Height + tLn) * 2 + sizeCol.Width + tLn + tRcr * Math.PI; break;
                    default: this.Ucrit_ex = sizeCol.Height + sizeCol.Width + tLn * 2 + tRcr * Math.PI * 0.5; break;
                }
                tRcr = this.Dcrit_in; //радиус скругления углов  при внутреннем критическом периметре
                switch (posColumn)
                {
                    case position.center: this.Ucrit_in = sizeCol.Height * 2 + sizeCol.Width * 2 + tRcr * Math.PI * 2; break;
                    case position.outside: this.Ucrit_in = sizeCol.Height * 2 + sizeCol.Width  + tRcr * Math.PI; break;
                    default: this.Ucrit_in = sizeCol.Height + sizeCol.Width + tRcr * Math.PI * 0.5; break;
                }
                return;
            }
        }
        void calcVsd()//вычисление Vsd результирующая поперечная сила
        {
            if(Ucrit_ex>0)
                switch (posColumn)
                {
                    case position.center: this.Vsd_ex = 1000*1.15 * this.load / Ucrit_ex; break;
                    case position.outside: this.Vsd_ex = 1000*1.4 * this.load / Ucrit_ex; break;
                    case position.corner: this.Vsd_ex = 1000*1.5 * this.load / Ucrit_ex; break;
                }
            if (Ucrit_in > 0)
                switch (posColumn)
                {
                    case position.center: this.Vsd_in = 1000 * 1.15 * this.load / Ucrit_in; break;
                    case position.outside: this.Vsd_in = 1000 * 1.4 * this.load / Ucrit_in; break;
                    case position.corner: this.Vsd_in = 1000 * 1.5 * this.load / Ucrit_in; break;
                }
        }
        public int checkVsd_Vrdc()// проверка условия прочности бет сечения без попер. армирования
        {
            this.ReciveDataSlab();
            this.calcUcrits();
            this.calcVsd();
            double _Vrdc_in = this.VRdc(this.WorkHeight);
            double _Vrdc_ex = this.VRdc(this.WorkHeight - this.thicknessCap);
            
            if (this.thicknessCap == 0)// случай без капители
            {
                if (_Vrdc_in > Vsd_ex*perStr) // 1- прочность бет. сечения без арматуры обеспечена
                    { this.SendDataSlab(this.WorkHeight); return 1;}
                if (_Vrdc_in * 1.5 > Vsd_ex * perStr)//0- или поставте арматуру или увеличте толщину плиты
                    { this.SendDataSlab(this.WorkHeight); return 0;}
                this.SendDataSlab(this.WorkHeight); return -1;  //-1 невозможно установить арматуру, только увеличение толщины плиты
            }
            if (this.thicknessCap > 0 && this.Ucrit_in==0)// случай с капителью и только с внешним крит. периметром
            {
                if (_Vrdc_ex > Vsd_ex * perStr) // 1- прочность бет. сечения(капители) без арматуры обеспечена
                    {this.SendDataSlab(this.WorkHeight-this.thicknessCap); return 1; }
                //if (this.VRdc(this.WorkHeight-this.thicknessCap) * 1.5 > Vsd_ex)//0- или поставте арматуру в капитель или увеличте толщину плиты
                //    { this.SendDataSlab(this.WorkHeight - this.thicknessCap); return 0; }
                this.SendDataSlab(this.WorkHeight - this.thicknessCap); return 2;   //-2 невозможно установить установить арматуру, увеличте ширину капители
            }
            if (this.thicknessCap > 0 && this.Ucrit_in > 0)// случай с капителью + внешним и внутр. крит. периметр
            {
                if (_Vrdc_ex > Vsd_ex * perStr &&
                    _Vrdc_in > Vsd_in * perStr) // 1- прочность бет. сечения(капители) без арматуры обеспечена
                    {this.SendDataSlab(this.WorkHeight - this.thicknessCap); return 1; }
                if (_Vrdc_ex > Vsd_ex * perStr &&
                    _Vrdc_in * 1.5 > Vsd_in * perStr)//-3- или поставте арматуру в капитель или увеличте толщину капители
                    {this.SendDataSlab(this.WorkHeight - this.thicknessCap); return 3; }
                if (_Vrdc_ex < Vsd_ex * perStr && _Vrdc_in > Vsd_in * perStr) // 4- только увеличить ширину капители
                { this.SendDataSlab(this.WorkHeight - this.thicknessCap); return 4; }
                if (_Vrdc_ex > Vsd_ex * perStr &&
                    _Vrdc_in * 1.5 < Vsd_in * perStr)//-6-только увеличте толщину капители
                { this.SendDataSlab(this.WorkHeight - this.thicknessCap); return 6; }
                this.SendDataSlab(this.WorkHeight - this.thicknessCap); return 5;  //-1 невозможно установить арматуру, только увеличение толщины плиты и ширины
            }
            return -7; //ошибка. ниодно из условий не подолшло
        }
        ////////////////////////////////////////////////////
        //расчеты плиты, армированной поперченой арматурой//
        ////////////////////////////////////////////////////
        public int checkVsd_0() // проверка  плит с попер армированием из условия раздавливания
        {
            switch (posColumn)
            {
                case position.center: this.U0 = this.sizeCol.Height * 2 + this.sizeCol.Width * 2; break;
                case position.outside: this.U0 = Math.Min(this.sizeCol.Width + 3 * WorkHeight, this.sizeCol.Width + 2 * this.sizeCol.Height); break;
                case position.corner: this.U0 = Math.Min(this.WorkHeight * 3, this.sizeCol.Height + this.sizeCol.Width); break;
            }
            double tV = 0.6 * (1 - this.valueFck / 250);// кофф.
            double t1, t2;
            t1 = this.load * 1000 / this.U0;
            t2 = 0.1 * 0.5 * tV * valueFck * WorkHeight;
            if(t1 <t2)
                return 1;
            return 0;
        }
        double RoSwMin//  мин коэфф армирования
        {
            get { return 0.16 * Fctm / (this.valueStrngSt==348?500.0:240.0); }
        }
        double calcU_i(double _Sw_tot)// вычисление i-го периметра
        {
            switch (posColumn)
            {
                case position.center: return sizeCol.Height * 2 + sizeCol.Width * 2 + _Sw_tot  * Math.PI * 2;
                case position.outside: return sizeCol.Height * 2 + sizeCol.Width + _Sw_tot  * Math.PI;
                default: return sizeCol.Height + sizeCol.Width + _Sw_tot *  Math.PI * 0.5;
            }
        }
        uint MinN_Elemnts(byte _Ni, double _Sw)// минимальное число стержней в i-м периметре
        {
            return (uint)Math.Ceiling(this.calcU_i(0.5 * this.WorkHeight + (_Ni - 1) * _Sw) / (1.5 * this.WorkHeight));
        }
        double calcVrd_sy_i(uint N_elem, byte _Ni, double _Sw)// вычисления прочности i-го периметра(только в пределах капители, если она есть)
        { 
            double _Vrdc= this.VRdc(this.WorkHeight);
            double Aswi=this.valueSqwSt*N_elem;
            double temp = _Vrdc + Aswi * 100 * 0.1 * this.valueStrngSt / this.calcU_i(0.5 * this.WorkHeight + (_Ni - 1) * _Sw);
            return temp;
        }
        double calcVsd_i(byte _Ni, double _Sw, double L)// вычисление погонной поперечной силы т/м в i-м периметре(L=0.5 для Vsd,a=0,5+1,5)
        {
            double radiusU = L * this.WorkHeight + (_Ni - 1) * _Sw;
            if ((radiusU >= ContentDataSlab._capCol.Height - ContentDataSlab._sizeCol.Height) && (thicknessCap != 0) && (L != 2))
                MessageBox.Show("Asw выходит за пределы капители!");
            double Ui = this.calcU_i(radiusU);
            switch (posColumn)
            {
                case position.center: return 1000 * 1.15 * this.load / Ui;
                case position.outside: return 1000 * 1.4 * this.load / Ui;
                default: return 1000 * 1.5 * this.load / Ui;
            }
        }
        double calcVrd_ca(byte _Ni, double _Sw)  // прочность на продавление крайнего периметра
        {     
            double tWorkH;
            double radiusU = 2 * this.WorkHeight + (_Ni - 1) * _Sw;
            if ((radiusU >= ContentDataSlab._capCol.Height - ContentDataSlab._sizeCol.Height) && (thicknessCap != 0))
                tWorkH = this.WorkHeight - this.thicknessCap;
            else
                tWorkH = this.WorkHeight;
            double tVrd_ca = 0.75 * 0.15 * this.K(tWorkH) * Math.Pow((100 * this.PL(tWorkH) * this.valueFck), 1.0 / 3.0) * (tWorkH) * 0.1;
            return tVrd_ca;  
        }
        double RoSw_i(uint N_elem, byte _Ni, double _Sw)// коэфф армирования в i-м периметре
        {
            double Aswi = this.valueSqwSt * N_elem;
            return Aswi*100/(_Sw*this.calcU_i(0.5 * this.WorkHeight + (_Ni - 1) * _Sw));
        }
        //////////////////////////////////////////////////////////////////
        //подбор параметров для плиты, армированной поперченой арматурой//
        //////////////////////////////////////////////////////////////////
        public List<PerimetrSteel> Auto_check()
        {
            byte nmb_perim;
            uint nmb_elem;
            double lim_Sw = 0.75 * this.WorkHeight;
            for (nmb_perim = 1; nmb_perim <= 15; nmb_perim++)   //подбираем количество периметров, исходя из условия по Крайнему периметру
            {
                double temp = this.calcVsd_i(nmb_perim, lim_Sw, 2);
                if (temp < this.calcVrd_ca(nmb_perim, lim_Sw))
                    break;
            }
            slab1_cont = new List<PerimetrSteel>(nmb_perim+1);  //создаем контейнер для хранения данных о периметрах
            for (byte i = 1; i <= nmb_perim; i++)// перебор периметров
            {
                nmb_elem = this.MinN_Elemnts(i, lim_Sw);
                while(!(this.calcVrd_sy_i(nmb_elem,i,lim_Sw)>this.calcVsd_i(i,lim_Sw,0.5))||!(this.RoSw_i(nmb_elem,i,lim_Sw)>this.RoSwMin))//подбор количества стержней в перимтре
                {
                    nmb_elem++;
                }
                slab1_cont.Add(new PerimetrSteel(nmb_elem, this.calcU_i(0.5 * this.WorkHeight + (i - 1) * lim_Sw), this.calcVrd_sy_i(nmb_elem, i, lim_Sw),
                    this.calcVsd_i(i, lim_Sw, 0.5), 0.5 * this.WorkHeight + (i - 1) * lim_Sw));//добавляем в список элементы обькта класса PerimetrSteel
            }
            slab1_cont.Add(new PerimetrSteel(0, this.calcU_i(2 * this.WorkHeight + (nmb_perim - 1) * lim_Sw), this.calcVrd_ca(nmb_perim, lim_Sw), this.calcVsd_i(nmb_perim, lim_Sw, 2), 2 * this.WorkHeight + (nmb_perim - 1) * lim_Sw));
            return slab1_cont;
        }
    }

    public class DrawReport 
    {   
        List<PerimetrSteel> perInfo;
        PictureBox pbox;
        Graphics graph;
        Bitmap bMap;
        public DrawReport(List<PerimetrSteel> _perInfo, PictureBox _pbox)
        {
            this.perInfo = _perInfo;
            this.pbox = _pbox;
            this.bMap = new Bitmap(_pbox.Size.Width, _pbox.Size.Height);
            this.graph = Graphics.FromImage(this.bMap);
        }
        void DrawPerim(int w, int h, int r, Color color, int thickness_pen, int numbAsw, double uPerim) //рисовать периметры со стержнями
        {
            //создаем холст
            uPerim += 1;
            GraphicsPath perGraph = new GraphicsPath();
            float x_center = (float)Math.Round(this.pbox.Size.Width * 0.5, 0);
            float y_center = (float)Math.Round(this.pbox.Size.Height * 0.5, 0);

            Point p1, p2;
            //делаем линию справа
            p1 = new Point((int)(x_center + w * 0.5), (int)(y_center - 0.5 * h + r));
            p2 = new Point((int)(x_center + w * 0.5), (int)(y_center + 0.5 * h - r));
            perGraph.AddLine(p1, p2);
            //угол справа низ
            p1 = new Point((int)(x_center + w * 0.5 - 2 * r), (int)(y_center + 0.5 * h - 2 * r));
            perGraph.AddArc(p1.X, p1.Y, 2 * r, 2 * r, 0, 90);
            //делаем линию снизу
            p1 = new Point((int)(x_center - w * 0.5 + r), (int)(y_center + 0.5 * h));
            p2 = new Point((int)(x_center + w * 0.5 - r), (int)(y_center + 0.5 * h));
            perGraph.AddLine(p2, p1);
            //угол слева низ
            p1 = new Point((int)(x_center - w * 0.5), (int)(y_center + 0.5 * h - 2 * r));
            if (ContentDataSlab._posColumn == position.outside || ContentDataSlab._posColumn == position.center)
                perGraph.AddArc(p1.X, p1.Y, 2 * r, 2 * r, 90, 90);
            //делаем линию слева
            p1 = new Point((int)(x_center - w * 0.5), (int)(y_center - 0.5 * h + r));
            p2 = new Point((int)(x_center - w * 0.5), (int)(y_center + 0.5 * h - r));
            if (ContentDataSlab._posColumn == position.outside || ContentDataSlab._posColumn == position.center)
                perGraph.AddLine(p2, p1);
            // четверть круга слева вверху
            p1 = new Point((int)(x_center - w * 0.5), (int)(y_center - 0.5 * h));
            if (ContentDataSlab._posColumn == position.center)
                perGraph.AddArc(p1.X, p1.Y, 2 * r, 2 * r, 180, 90);
            //делаем линию сверху
            p1 = new Point((int)(x_center - w * 0.5 + r), (int)(y_center - 0.5 * h));
            p2 = new Point((int)(x_center + w * 0.5 - r), (int)(y_center - 0.5 * h));
            if (ContentDataSlab._posColumn == position.center)
            perGraph.AddLine(p1, p2);
            //угол справа вверх
            p1 = new Point((int)(x_center + w * 0.5 - 2 * r), (int)(y_center - 0.5 * h));
            if (ContentDataSlab._posColumn == position.center)
            perGraph.AddArc(p1.X, p1.Y, 2 * r, 2 * r, 270, 90);
            //perGraph.CloseFigure();
            // наносим на хост
            Pen pen = new Pen(color, thickness_pen);
            this.graph.DrawPath(pen, perGraph);
            perGraph.Dispose();
            if (numbAsw == 0)
                return;
            //далее наносим точки(стержни Asw) на периметр
            GraphicsPath perDots = new GraphicsPath();
            double stepAsw = uPerim / numbAsw; //шаг стержней
            double Lcirc = 0.5 * Math.PI * r; // длина  четверти окружности
            float x_arc, y_arc; // координаты точек (стержней) при смещении по скруглению углов периметра
            double L_Over=0;//перебор длины
            double n = 0;

            //для правой стороны
            while (n <= (h - 2 * r))
            {
                perDots.AddEllipse((int)Math.Round(x_center - 1 + 0.5 * w), (int)Math.Round(y_center - 1 - 0.5 * h + r + n), 2, 2);
                n += stepAsw;
                L_Over = n - (h - 2 * r);
            }
            //для нижнего правого угла
            x_arc = (float)(x_center + w * 0.5 - r);
            y_arc = (float)(y_center + h * 0.5 - r);
            n = L_Over;
            while (n <= Lcirc)
            {
                double arc = n / r;
                perDots.AddEllipse((int)(x_arc - 1 + r * Math.Cos(arc)), (int)(y_arc - 1 + r * Math.Sin(arc)), 2, 2);
                n += stepAsw;
                L_Over = n - Lcirc;
            }
            //для низа
            n = L_Over;
            while (n <= (w - 2 * r))
            {
                perDots.AddEllipse((int)(x_center - 1 + 0.5 * w - r - n), (int)(y_center - 1 + 0.5 * h), 2, 2);
                n += stepAsw;
                L_Over = n - (w - 2 * r);
            }
            //для нижнего левого угла
            x_arc = (float)(x_center - w * 0.5 + r);
            y_arc = (float)(y_center + h * 0.5 - r);
            n = L_Over;
            if (ContentDataSlab._posColumn == position.outside || ContentDataSlab._posColumn == position.center)
            while (n <= Lcirc)
            {
                double arc = n / r;
                perDots.AddEllipse((int)(x_arc - 1 - r * Math.Sin(arc)), (int)(y_arc + 1 + r * Math.Cos(arc)), 2, 2);
                n += stepAsw;
                L_Over = n - Lcirc;
            }
            n = L_Over;
            //для слева вертикальной
            if (ContentDataSlab._posColumn == position.outside || ContentDataSlab._posColumn == position.center)
            while (n <= (h - 2 * r))
            {
                perDots.AddEllipse((int)Math.Round(x_center - 1 - 0.5 * w), (int)Math.Round(y_center - 1 + 0.5 * h - r - n), 2, 2);
                n += stepAsw;
                L_Over = n - (h - 2 * r);
            } 
            //для верхнего левого угла
            x_arc = (float)(x_center - w * 0.5 + r);
            y_arc = (float)(y_center - h * 0.5 + r);
            n=L_Over;
            if (ContentDataSlab._posColumn == position.center)
            while(n <= Lcirc)
            {
                double arc = n / r;
                perDots.AddEllipse((int)(x_arc - 1 - r * Math.Cos(arc)), (int)(y_arc - 1 - r * Math.Sin(arc)), 2, 2);
                n += stepAsw;
                L_Over = n - Lcirc;
            }
            //для верха
            n=L_Over;
            if (ContentDataSlab._posColumn == position.center)
            while( n < (w - 2 * r))
            {
                perDots.AddEllipse((int)(x_center - 1 - 0.5 * w + r + n), (int)(y_center - 1 - 0.5 * h), 2, 2);
                n += stepAsw;
                L_Over = n - (w - 2 * r);
            }   
            //для верхнего правого угла
            x_arc = (float)(x_center + w * 0.5 - r);
            y_arc = (float)(y_center - h * 0.5 + r);
            n = L_Over;
            if (ContentDataSlab._posColumn == position.center)
            while (n < Lcirc)
            {
                double arc = n / r;
                perDots.AddEllipse((int)(x_arc - 1 + r * Math.Sin(arc)), (int)(y_arc - 1 - r * Math.Cos(arc)), 2, 2);
                n += stepAsw;
            }
            //рисуем путь на холст
            pen = new Pen(Color.Red, 2);
            this.graph.DrawPath(pen, perDots);
            perDots.Dispose();
        }
        //рисуем размерные линии периметров
        void DrawSizeLine(int w, int h, int numberPer, double scale, double sw_tot)
        {
            GraphicsPath sizeLineGraph = new GraphicsPath();
            float x_center = (float)Math.Round(this.pbox.Size.Width * 0.5, 0);
            float y_center = (float)Math.Round(this.pbox.Size.Height * 0.5, 0);
            int Wmax = (int)(perInfo[perInfo.Capacity - 1]._Swtot * scale + ContentDataSlab._sizeCol.Width * 0.5 * scale);//макс ширина, от которого начинать строить размерные линии
            int Hmax = (int)(perInfo[perInfo.Capacity - 1]._Swtot * scale + ContentDataSlab._sizeCol.Height * 0.5 * scale);//макс высота, от которого начинать строить размерные линии
            int stepSize = 15 * numberPer;
            //размерные линии горизонтальные
            Point p1;
            Point p2;
            switch(ContentDataSlab._posColumn)
            {
                case position.center:
                    p1 = new Point((int)(x_center - 0.5 * w - 5), (int)(y_center - Hmax - stepSize ));
                    p2 = new Point((int)(x_center + 0.5 * w + 5), (int)(y_center - Hmax - stepSize ));
                    break;
                case position.outside:
                    p1 = new Point((int)(x_center - 0.5 * w - 5), (int)(y_center - 20 - stepSize ));
                    p2 = new Point((int)(x_center + 0.5 * w + 5), (int)(y_center - 20 - stepSize ));
                    break;
                default:
                    p1 = new Point((int)(x_center - 0.5 * w + sw_tot - 5), (int)(y_center - 20 - stepSize));
                    p2 = new Point((int)(x_center + 0.5 * w + 5), (int)(y_center  -20 - stepSize ));
                    break;
            }
            
            sizeLineGraph.AddLine(p1, p2);//гор линия
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p1.X, p1.Y + 5, p1.X + 10, p1.Y - 5);//засечка
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p2.X - 10, p2.Y + 5, p2.X, p2.Y - 5);//засечка
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p1.X + 5, p1.Y - 5, p1.X + 5, p1.Y + stepSize);
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p2.X - 5, p2.Y - 5, p2.X - 5, p2.Y + stepSize);
            sizeLineGraph.CloseFigure();
            FontFamily font1 = new FontFamily("Calibri");
            int fontStyle = (int)FontStyle.Regular;
            int emSize = 12;
            StringFormat strFormat = StringFormat.GenericDefault;
             Point pointTextSize;
            if(ContentDataSlab._posColumn==position.corner)
                pointTextSize = new Point((int)(x_center -10), p1.Y - 15);
            else
                pointTextSize = new Point((int)(x_center -20), p1.Y - 15);
            string valueSize;
            if (ContentDataSlab._posColumn == position.center || ContentDataSlab._posColumn == position.outside)//вычисляем размер в зависимости от расположения колонны
            {
                valueSize = Math.Floor((w / scale)).ToString() + " мм";
            }
            else
            {
                valueSize = Math.Floor(((w - sw_tot) / scale)).ToString() + " мм";
            }
            sizeLineGraph.AddString(valueSize, font1, fontStyle, emSize, pointTextSize, strFormat);
            Pen pen = new Pen(Color.DarkGray, 1);
            this.graph.DrawPath(pen, sizeLineGraph);
            SolidBrush br = new SolidBrush(Color.DarkGray);
            this.graph.FillPath(br, sizeLineGraph);
            sizeLineGraph.CloseFigure();

            //размерные вертикальные
            switch (ContentDataSlab._posColumn)
            {
                case position.center:
                   p1 = new Point((int)(x_center  -Wmax- stepSize), (int)(y_center - 0.5 * h - 5));
                   p2 = new Point((int)(x_center -Wmax -stepSize), (int)(y_center + 0.5 * h + 5));
                    break;
                case position.outside:
                    p1 = new Point((int)(x_center  - Wmax - stepSize), (int)(y_center - 0.5 * h + sw_tot - 5));
                    p2 = new Point((int)(x_center - Wmax - stepSize), (int)(y_center + 0.5 * h + 5));
                    break;
                default:
                    p1 = new Point((int)(x_center - 40 - stepSize), (int)(y_center - 0.5 * h + sw_tot - 5));
                    p2 = new Point((int)(x_center - 40 - stepSize), (int)(y_center + 0.5 * h + 5));
                    break;
            }  
            sizeLineGraph.AddLine(p1, p2);//гор линия
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p1.X + 5, p1.Y, p1.X - 5, p1.Y + 10);//засечка
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p2.X + 5, p2.Y - 10, p2.X - 5, p2.Y);//засечка
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p1.X - 5, p1.Y + 5, p1.X + stepSize, p1.Y + 5);   //гор. линия
            sizeLineGraph.CloseFigure();
            sizeLineGraph.AddLine(p2.X - 5, p2.Y - 5, p2.X + stepSize, p2.Y - 5); //гор. линия
            sizeLineGraph.CloseFigure();
            this.graph.DrawPath(pen, sizeLineGraph);
            sizeLineGraph.Dispose();
            //числовой размер
            GraphicsPath sizeGraph = new GraphicsPath();
            if (ContentDataSlab._posColumn == position.corner || ContentDataSlab._posColumn == position.outside)
                pointTextSize = new Point((int)(p1.X - 15), (int)(y_center + 30));
            else
                pointTextSize = new Point((int)(p1.X - 15), (int)(y_center + 20));
            if (ContentDataSlab._posColumn == position.center)//вычисляем размер в зависимости от расположения колонны
            {
                valueSize = Math.Floor((h / scale)).ToString() + " мм";
            }
            else
            {
                valueSize = Math.Floor(((h - sw_tot) / scale)).ToString() + " мм";
            }

            sizeGraph.AddString(valueSize, font1, fontStyle, emSize, pointTextSize, strFormat);
            Matrix mat = new Matrix();
            mat.RotateAt(270, pointTextSize);
            sizeGraph.Transform(mat);
            this.graph.DrawPath(pen, sizeGraph);
            this.graph.FillPath(br, sizeGraph);
            sizeGraph.Dispose();

        }
        void DrawColumn(float w, float h, Color color, int thickness)//рисуем колонну
        {
            GraphicsPath grb = new GraphicsPath();
            int x_center = (int)Math.Round(this.pbox.Size.Width * 0.5, 0);
            int y_center = (int)Math.Round(this.pbox.Size.Height * 0.5, 0);
            grb.AddRectangle(new Rectangle((int)(x_center - 0.5 * w), (int)(y_center - 0.5 * h), (int)w, (int)h));
            // наносим на хост
            Pen pen = new Pen(color, thickness);
            this.graph.DrawPath(pen, grb);
            grb.Dispose();
        }
      public void Draw(double scale)
        {
            int i = 0; 
            this.DrawColumn((int)(ContentDataSlab._sizeCol.Width * scale), (int)(ContentDataSlab._sizeCol.Height * scale), Color.Green, 3);
            foreach (PerimetrSteel temp in this.perInfo)
            {
                int _w = (int)Math.Round(ContentDataSlab._sizeCol.Width * scale + 2 * temp._Swtot * scale,0);
                int _h = (int)Math.Round(ContentDataSlab._sizeCol.Height * scale + 2 * temp._Swtot * scale,0);
                this.DrawPerim(_w, _h, (int)(temp._Swtot * scale), Color.Blue, 1, (int)temp._numb_elements, (int)(temp._length_per * scale));
                this.DrawSizeLine(_w, _h, ++i, scale, temp._Swtot * scale);
            }
            this.pbox.Image = bMap;
        }
    }
}