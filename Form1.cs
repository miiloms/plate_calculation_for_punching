﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Application_for_Calc_Punch_Building_
{

    public delegate void calcClicMethod(object sender, EventArgs e);
    [Serializable]
    struct DataFromMainForm
    {
       public  bool IsSetInterpol{set;get;}//set or get on or off interpolation
       public double FixWX { set; get; }
       public double FixWY { set; get; }
       public double FixAstX { set; get; }
       public double FixAstY { set; get; }
       public double PersStrong { set; get; }
       public string Acol { set; get; }
       public string Bcol { set; get; }
       public string Hsab { set; get; }
       public string Hcap { set; get; }
       public string Lcap { set; get; }
       public string Cx { set; get; }
       public string Cy{ set; get; }
       public string AstX { set; get; }
       public string AstY { set; get; }
       public int ClassConcrete { set; get; }
       public int ClassSteel { set; get; }
       public int DiamSteel { set; get; }
       public bool IsSetAsw { set; get; }
       public string Load { set; get; }
       public position PosCol { set; get; }
       public bool ModeCalc { set; get; }
       public string Comment { set; get; }
    }
    public partial class FrmStartWindow : Form
    {
        Slab slab_work;
        public  bool flag_interpol = false;
        public  double fixWX;
        public double fixWY;
        public double fixAstX;
        public double fixAstY;
        double persStrong;
        string savePath;
        DataFromMainForm dataSlab;
        public FrmStartWindow()
        {
            InitializeComponent();
            this.cmBox_classConcrete.SelectedIndex = 0;
            this.cmBox_ClassSteel.SelectedIndex = 1;
            this.cmBox_Diam_St.SelectedIndex = 0;
            slab_work = new Slab();
            FrmAutoCalc.delegCalcSolAuto += this.btnCalcSol_Click;
            persStrong = 1;
            this.tsSave.Enabled = false;
            this.tsBtnQickSave.Enabled = false;
            this.tsOpenFile.Click += new EventHandler(this.tsOpenFile_Click);
            this.FormClosing += new FormClosingEventHandler(this.form_Closing);
            this.tsBtnMode.AllowDrop = false;
        }

        void tsOpenFile_Click(object sender, EventArgs e)
        {
            this.OpenFileData();
        }
        void SerializeData()
        {
            if (savePath != "")
            {
                FileStream fs_save = new FileStream(savePath, FileMode.Create, FileAccess.Write);
                BinaryFormatter binFor = new BinaryFormatter();
                binFor.Serialize(fs_save, dataSlab);
                fs_save.Close();
                this.tsSave.Enabled = true;
            }
        }
        DateTime DeSeriazeData()
        {
            FileStream fs_open = new FileStream(savePath, FileMode.Open, FileAccess.Read);
            BinaryFormatter binFor = new BinaryFormatter();
            this.dataSlab = (DataFromMainForm)binFor.Deserialize(fs_open);
            fs_open.Close();
            FileInfo fInf = new FileInfo(savePath);
            return fInf.LastWriteTime;
        }
        void OpenFileData()
        {
            
            OpenFileDialog fileOpen = new OpenFileDialog();
            fileOpen.Filter = "Slab|*.slb";
            fileOpen.Title = "Открыть файл Slab...";
            fileOpen.ShowDialog();
            if (fileOpen.FileName != "")
            {
                DateTime temtDT;
                savePath = fileOpen.FileName;
                dataSlab = new DataFromMainForm();
                temtDT=this.DeSeriazeData();
                this.Text = "Расчет на продавливание ж.б. плит по СНБ 5.03.01-02 (ver. 1.0.0) - "+"[" + Path.GetFileName(this.savePath) + "]-" + temtDT.ToShortDateString() + " " + temtDT.ToShortTimeString() ;
                this.tsSave.Enabled = true;
                this.tsBtnQickSave.Enabled = true;
            }
            else
                return;
            this.flag_interpol=dataSlab.IsSetInterpol;
            this.chbUseAsw.Checked=dataSlab.IsSetAsw;
            if (dataSlab.ModeCalc == false)
                SetWithCap();   
            else
                SetWoutCap();

            this.tBox_acol.Text=dataSlab.Acol;
            this.tBox_bcol.Text=dataSlab.Bcol;
            this.tBoxMain_Astx.Text=dataSlab.AstX;
            this.tBoxMain_Asty.Text=dataSlab.AstY;
            this.cmBox_classConcrete.SelectedIndex=dataSlab.ClassConcrete;
            this.cmBox_ClassSteel.SelectedIndex=dataSlab.ClassSteel;
            this.tBox_Cx.Text=dataSlab.Cx;
            this.tBox_Cy.Text=dataSlab.Cy;
            this.cmBox_Diam_St.SelectedIndex=dataSlab.DiamSteel;
            this.tBox_Hcap.Text=dataSlab.Hcap;
            this.tBox_Hpl.Text= dataSlab.Hsab;
            this.tBox1_Ln.Text=dataSlab.Lcap;
            this.tBoxLoad.Text=dataSlab.Load;
            this.persStrong=dataSlab.PersStrong;
            this.fixAstX=dataSlab.FixAstX;
            this.fixAstY=dataSlab.FixAstY;
            this.fixWX=dataSlab.FixWX;
            this.fixWY=dataSlab.FixWY;
            this.rTextBoxComments.Text = dataSlab.Comment == null ? "Введите комментарий к расчету...\n" : dataSlab.Comment;
            if (dataSlab.PosCol == position.center)
            {
                this.pBox_Center.BackColor = Color.LightBlue;
                this.pBox_Corner.BackColor = Color.White;
                this.pBox_Outside.BackColor = Color.White;
            }
            if (dataSlab.PosCol == position.outside)
            {
                this.pBox_Center.BackColor = Color.White;
                this.pBox_Corner.BackColor = Color.White;
                this.pBox_Outside.BackColor = Color.LightBlue;
            }
            if (dataSlab.PosCol == position.corner)
            {
                this.pBox_Center.BackColor = Color.White;
                this.pBox_Corner.BackColor = Color.LightBlue;
                this.pBox_Outside.BackColor = Color.White;
            }
        }
        void SaveAsDataForm(bool qick_save)
        {
            dataSlab = new DataFromMainForm();
            dataSlab.IsSetInterpol=this.flag_interpol;
            dataSlab.IsSetAsw = this.chbUseAsw.Checked;
            dataSlab.ModeCalc = this.TSMenuWoutCap.Checked;
            dataSlab.Acol = this.tBox_acol.Text;
            dataSlab.Bcol = this.tBox_bcol.Text;
            dataSlab.AstX = this.tBoxMain_Astx.Text;
            dataSlab.AstY = this.tBoxMain_Asty.Text;
            dataSlab.ClassConcrete = this.cmBox_classConcrete.SelectedIndex;
            dataSlab.ClassSteel = this.cmBox_ClassSteel.SelectedIndex;
            dataSlab.Cx = this.tBox_Cx.Text;
            dataSlab.Cy = this.tBox_Cy.Text;
            dataSlab.DiamSteel = this.cmBox_Diam_St.SelectedIndex;
            dataSlab.Hcap = this.tBox_Hcap.Text;
            dataSlab.Hsab = this.tBox_Hpl.Text;
            dataSlab.Lcap = this.tBox1_Ln.Text;
            dataSlab.Load = this.tBoxLoad.Text;
            dataSlab.PersStrong = this.persStrong;
            dataSlab.FixAstX = this.fixAstX;
            dataSlab.FixAstY = this.fixAstY;
            dataSlab.FixWX = this.fixWX;
            dataSlab.FixWY = this.fixWY;
            dataSlab.Comment = this.rTextBoxComments.Text;
            foreach(PictureBox pb in this.grBox_LocationColumn.Controls)
            {
                if (pb.Equals(this.pBox_Center) && pb.BackColor == Color.LightBlue)
                { dataSlab.PosCol = position.center; break; }
                if (pb.Equals(this.pBox_Corner) && pb.BackColor == Color.LightBlue)
                { dataSlab.PosCol = position.corner; break; }
                if (pb.Equals(this.pBox_Outside) && pb.BackColor == Color.LightBlue)
                { dataSlab.PosCol = position.outside; break; }
            }

            /////saving
            if (qick_save)  //если быстрое сохранение, то не выводить диалог
            { this.SerializeData(); return; }
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter="Slab|*.slb";
            saveDialog.Title = "Сохранить как SLAB...";
            saveDialog.ShowDialog();
            this.savePath=saveDialog.FileName;
            this.SerializeData();
            if (this.savePath != "")
            {
                this.Text = "[" + Path.GetFileName(this.savePath) + "] - Расчет на продавливание ж.б. плит по СНБ 5.03.01-02 (ver. 1.0.0)";
            }
        }
       public  void GatheringDataForm(object sender)   //  сбор данных с формы, их проверка и доставка в буфер обмена
        {
            int tempW = 0, tempH = 0, tempHl = 0, Cx=0, Cy=0;
            double temp = 0;

            if (!int.TryParse(tBox_acol.Text, out tempW) || tempW <= 0)
            {
                tBox_acol.Focus(); throw new Exception("Неверно введен размер [a] колонны!");
            }
            if (!int.TryParse(tBox_bcol.Text, out tempH)|| tempH <= 0)
            {
                tBox_bcol.Focus(); throw new Exception("Неверно введен размер [b] колонны!");
            }
            ContentDataSlab._sizeCol = new Size(tempW, tempH);

            if (!double.TryParse(tBox_Hpl.Text, out temp)|| temp <= 0)
            { tBox_Hpl.Focus(); throw new Exception("Неверно введена толщина плиты!"); }
            ContentDataSlab._thicknessSlab = temp;


            if (!double.TryParse(tBox_Hcap.Text, out temp)|| temp < 0)
                if(this.TSMenuWithCap.Checked&&temp<=0)
            { tBox_Hcap.Focus(); throw new Exception("Неверно введена толщина капители!"); }
            ContentDataSlab._thicknessCap = temp;

            if (!int.TryParse(tBox1_Ln.Text, out tempHl)|| tempHl < 0)
                if (this.TSMenuWithCap.Checked && temp <= 0)
                { tBox1_Ln.Focus(); throw new Exception("Неверно введен выступ капители!"); }
            if (tBox_acol.Text != "" && tBox_bcol.Text != "")
                ContentDataSlab._capCol = new Size(2 * tempHl + ContentDataSlab._sizeCol.Width, 2 * tempHl + ContentDataSlab._sizeCol.Height);

            if (!int.TryParse(this.tBox_Cx.Text, out Cx)||Cx>=ContentDataSlab._thicknessSlab*0.5|| Cx <= 0)
                throw new Exception("Неверно введено расстояние до ц.т. арматуры по Х!");
            if (!int.TryParse(this.tBox_Cy.Text, out Cy) || Cy >= ContentDataSlab._thicknessSlab * 0.5|| Cy <= 0)
                throw new Exception("Неверно введено расстояние до ц.т. арматуры по Y!");
            ContentDataSlab._coverConcrete = new Point(Cx, Cy);


            if ((!double.TryParse(tBoxLoad.Text, out temp) || temp <= 0) && (!sender.Equals(this.tBoxMain_Astx) && !sender.Equals(this.tBoxMain_Asty)&&sender.Equals(this.btnCalcSol)))
            { tBoxLoad.Focus(); throw new Exception("Неверно введена нагрузка P!"); }
            ContentDataSlab._load = temp;

            if ((!double.TryParse(this.tBoxMain_Astx.Text, out temp) || temp <= 0) && (!sender.Equals(this.tBoxMain_Astx) && !sender.Equals(this.tBoxMain_Asty)))
            {
                this.tBoxMain_Astx.Focus(); throw new Exception("Неверно введена площадь продольного армирования по X!");
            }
            ContentDataSlab._astX = temp;
            if ((!double.TryParse(this.tBoxMain_Asty.Text, out temp) || temp <= 0) && (!sender.Equals(this.tBoxMain_Astx) && !sender.Equals(this.tBoxMain_Asty)))
            {
                this.tBoxMain_Asty.Focus(); throw new Exception("Неверно введена площадь продольного армирования по Y!");
            }
            ContentDataSlab._astY = temp;

            switch (this.cmBox_classConcrete.SelectedIndex)
            {
                case 0: ContentDataSlab._valueFck = ClassConcrete.C16_20; break;
                case 1: ContentDataSlab._valueFck = ClassConcrete.C20_25; break;
                case 2: ContentDataSlab._valueFck = ClassConcrete.C25_30; break;
                case 3: ContentDataSlab._valueFck = ClassConcrete.C30_37; break;
                case 4: ContentDataSlab._valueFck = ClassConcrete.C35_45; break;
                case 5: ContentDataSlab._valueFck = ClassConcrete.C40_50; break;
            }

            switch (this.cmBox_ClassSteel.SelectedIndex)
            {
                case 0: ContentDataSlab._valueStrngSt = ClassSteel.S500; break;
                case 1: ContentDataSlab._valueStrngSt = ClassSteel.S240; break;
            }

            switch (this.cmBox_Diam_St.SelectedIndex)
            {
                case 0: ContentDataSlab._valueSqwSt = SqwSteel.D6; break;
                case 1: ContentDataSlab._valueSqwSt = SqwSteel.D8; break;
                case 2: ContentDataSlab._valueSqwSt = SqwSteel.D10; break;
                case 3: ContentDataSlab._valueSqwSt = SqwSteel.D12; break;
                case 4: ContentDataSlab._valueSqwSt = SqwSteel.D14; break;
            }

            if (pBox_Center.BackColor == Color.LightBlue)
                ContentDataSlab._posColumn = position.center;
            if (pBox_Outside.BackColor == Color.LightBlue)
                ContentDataSlab._posColumn = position.outside;
            if (pBox_Corner.BackColor == Color.LightBlue)
                ContentDataSlab._posColumn = position.corner;
            ContentDataSlab._reservStrong = persStrong;
        }
        private void btnCoverLayer_Click(object sender, EventArgs e)
        {
            FrmCoverLayer frmCovLayer = new FrmCoverLayer();
            frmCovLayer.ShowDialog();
        }
        private void tBoxMain_Ast_Click(object sender, EventArgs e)
        {
            try
            {
            this.GatheringDataForm(sender);
            FrmSqwSteel_inline FormSqw = new FrmSqwSteel_inline(this, this.slab_work);
            FormSqw.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pBox_Side_Click(object sender, EventArgs e)// визуальные кнопки для выбора расположения колонн
        {
            if(sender.Equals(this.pBox_Center))
            {
                this.pBox_Center.BackColor = Color.LightBlue;
                this.pBox_Corner.BackColor = Color.White;
                this.pBox_Outside.BackColor = Color.White;
                return;
            }
            if (sender.Equals(this.pBox_Outside))
            {
                this.pBox_Center.BackColor = Color.White;
                this.pBox_Corner.BackColor = Color.White;
                this.pBox_Outside.BackColor = Color.LightBlue;
                return;
            }
            if (sender.Equals(this.pBox_Corner))
            {
                this.pBox_Center.BackColor = Color.White;
                this.pBox_Corner.BackColor = Color.LightBlue;
                this.pBox_Outside.BackColor = Color.White;
                return;
            }
        }

        private void tBox_TextChanged(object sender, EventArgs e)
        {
            if ((this.tBoxMain_Astx.Text != "" || this.tBoxMain_Asty.Text != "") && !flag_interpol) //если нет авто интреполяции
            {
                this.tlStrStatL.Text = " Внимание! Параметры расчета изменены требуеться пересмотреть Ast,x и Ast,y";
                this.tlStrStatL.BackColor = Color.SteelBlue;
                this.tBoxMain_Astx.BackColor = Color.SteelBlue;
                this.tBoxMain_Asty.BackColor = Color.SteelBlue;
                return;
            }    
        }

        void SetWithCap()
        {
            this.TSMenuWoutCap.Checked = false;
            this.TSMenuWithCap.Checked = true;
            this.tBox_Hcap.Enabled = true;
            this.tBox1_Ln.Enabled = true;
            tsBtnModeSlab.Checked = TSMenuWoutCap.Checked;
            tsBtnModeCap.Checked = TSMenuWithCap.Checked;
            this.tlSStatusLMode.Text = "Режим расчета: ПЛИТА + КАПИТЕЛЬ";
        }
        void SetWoutCap()
        {
            this.TSMenuWoutCap.Checked = true;
            this.TSMenuWithCap.Checked = false;
            this.tBox_Hcap.Enabled = false;
            this.tBox1_Ln.Enabled = false;
            this.tBox_Hcap.Text = "0";
            this.tBox1_Ln.Text = "0";
            tsBtnModeSlab.Checked = TSMenuWoutCap.Checked;
            tsBtnModeCap.Checked = TSMenuWithCap.Checked;
            this.tlSStatusLMode.Text = "Режим расчета: ПЛИТА";
        }
        private void TSMenuWoutCap_Click(object sender, EventArgs e)
        {
            SetWoutCap();
        }

        private void TSMenuWithCap_Click(object sender, EventArgs e)
        {
            SetWithCap();
            this.tBox_Hcap.Text = "";
            this.tBox1_Ln.Text = "";
        }

        private void btnCalcSol_Click(object sender, EventArgs e)
        {
            try
            {
                if (!sender.Equals(this.btnCalcSol))
                    this.persStrong = 1;
                this.GatheringDataForm(sender);
                FrmReport report = new FrmReport(this.slab_work, this.chbUseAsw.Checked, sender);
                report.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tBox_Leave(object sender, EventArgs e)
        {
            if (flag_interpol) //если есть автоинтерполяция то интерполируем новые значения прод арматуры
            {
                try
                {
                    this.GatheringDataForm(sender);
                    this.slab_work.ReciveDataSlab();
                    this.tBoxMain_Astx.Text = ((this.fixAstX/ this.fixWX) * slab_work.WidthX1(slab_work.WorkHeight)).ToString();
                    this.tBoxMain_Asty.Text = ((this.fixAstY / this.fixWY) * slab_work.WidthY1(slab_work.WorkHeight)).ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnAutoSol_Click(object sender, EventArgs e)
        {
            double temp;
            try
            {
                if ((!double.TryParse(tBoxLoad.Text, out temp) || temp <= 0))
                { tBoxLoad.Focus(); throw new Exception("Неверно введена нагрузка P!"); }
                ContentDataSlab._load = temp;
                if (flag_interpol == false)
                {
                    this.GatheringDataForm(sender);
                    FrmSqwSteel_inline FormSqw = new FrmSqwSteel_inline(this, this.slab_work);
                    FormSqw.Show();
                    FormSqw.chBox_Interpol.Checked = true;
                    FormSqw.btnOk_Click(sender,e);
                }
                FrmAutoCalc frmAuto = new FrmAutoCalc(this.slab_work, this);
                frmAuto.ShowDialog();
                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); return;
            }
        }
        public void AcceptData(bool SolvCap, bool useAsw, double hCap, double wCap, double _percStr)
        {
            if (SolvCap)
            {
                SetWithCap();
                this.tBox_Hcap.Text = hCap.ToString();
                this.tBox1_Ln.Text = wCap.ToString();
                this.tBox_Leave(this, new EventArgs());
            }
            else
                SetWoutCap();
            this.chbUseAsw.Checked = useAsw;
            this.tBox_Leave(this, new EventArgs());
            persStrong = 1+_percStr/100;
        }

        private void tsmSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                this.SaveAsDataForm(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tsSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.SaveAsDataForm(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tsExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void form_Closing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialRez;
            dialRez = MessageBox.Show("Хотите сохранить данные расчета?", "Внимание", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            switch(dialRez)
            {
                case DialogResult.Yes: this.SaveAsDataForm(savePath != null); break;
                case DialogResult.Cancel: e.Cancel = true; break;
            }
        }

        private void tsAbout_Click(object sender, EventArgs e)
        {
            AboutBox FrmAbout = new AboutBox();
            FrmAbout.ShowDialog();
        }

        private void tBox_acol_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (sender.Equals(this.tBox_acol))
                {
                    this.tBox_bcol.Focus(); return;
                }
                if (sender.Equals(this.tBox_bcol))
                {
                    this.tBox_Hpl.Focus(); return;
                }
                if (sender.Equals(this.tBox_Hpl))
                {
                    if (!this.tsBtnModeSlab.Checked)
                    { this.tBox_Hcap.Focus(); return; }
                    else { this.tBox_Cx.Focus(); return; }
                }
                if (sender.Equals(this.tBox_Hcap))
                {
                    this.tBox1_Ln.Focus(); return;
                }
                if (sender.Equals(this.tBox1_Ln))
                {
                    this.tBox_Cx.Focus(); return;
                }
                if (sender.Equals(this.tBox_Cx))
                {
                    this.tBox_Cy.Focus(); return;
                }
                if (sender.Equals(this.tBox_Cy))
                {
                    this.tBoxMain_Astx.Focus(); return;
                }
            }
                

        }

        private void tBoxMain_Astx_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.tBoxMain_Ast_Click(sender, e);
        }
        private void rTextBoxComments_MouseLeave(object sender, EventArgs e)
        {
            if (this.rTextBoxComments.Text == "")
                rTextBoxComments.Text = "Введите комментарий к расчету...\n";
        }

        private void rTextBoxComments_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.rTextBoxComments.Text == "Введите комментарий к расчету...\n")
                rTextBoxComments.Clear();
        }
    }
}
