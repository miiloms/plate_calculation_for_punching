﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Application_for_Calc_Punch_Building_
{
    public partial class FrmSqwSteel_inline : Form
    {
        FrmStartWindow WinStart;
        Slab sl;
        public FrmSqwSteel_inline()
        {
            InitializeComponent();
        }
        public FrmSqwSteel_inline(FrmStartWindow _WinStart, Slab _sl)
        {
            InitializeComponent();
            this.WinStart = _WinStart;
            this.sl = _sl;
        }
        private void FrmSqwSteel_inline_Load(object sender, EventArgs e)
        {
            sl.ReciveDataSlab();
            this.tBox_W_Astx.Text = sl.WidthX1(sl.WorkHeight).ToString();
            this.textBox1.Text = sl.WidthY1(sl.WorkHeight).ToString();
            this.tBox_Ast_x.Text = this.WinStart.tBoxMain_Astx.Text;
            this.tBox_Ast_y.Text = this.WinStart.tBoxMain_Asty.Text;
            this.label3.Text = this.tBox_W_Astx.Text;
            this.label6.Text = this.textBox1.Text;
            chBox_Interpol.Checked=this.WinStart.flag_interpol;
            this.tBox_Ast_x.Enabled = !chBox_Interpol.Checked;
            this.tBox_Ast_y.Enabled = !chBox_Interpol.Checked;
        }

        public void btnOk_Click(object sender, EventArgs e)
        {
            this.WinStart.tBoxMain_Astx.Text = this.tBox_Ast_x.Text;
            this.WinStart.tBoxMain_Asty.Text = this.tBox_Ast_y.Text;
            this.WinStart.tBoxMain_Astx.BackColor = Color.White;
            this.WinStart.tBoxMain_Asty.BackColor = Color.White;
            this.WinStart.tlStrStatL.BackColor = Color.WhiteSmoke;
            this.WinStart.tlStrStatL.Text = "";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chBox_Interpol_CheckedChanged(object sender, EventArgs e)
        {
            if (chBox_Interpol.Checked)
            {
                double.TryParse(this.tBox_W_Astx.Text, out this.WinStart.fixWX);
                double.TryParse(this.textBox1.Text, out this.WinStart.fixWY);
                double.TryParse(this.tBox_Ast_x.Text, out this.WinStart.fixAstX);
                double.TryParse(this.tBox_Ast_y.Text, out this.WinStart.fixAstY);

            }
            this.WinStart.flag_interpol = chBox_Interpol.Checked;
            this.tBox_Ast_x.Enabled = !chBox_Interpol.Checked;
            this.tBox_Ast_y.Enabled = !chBox_Interpol.Checked;
        }

        private void tBox_Ast_x_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                this.tBox_Ast_y.Focus();
        }

    }
}
