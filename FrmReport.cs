﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Application_for_Calc_Punch_Building_
{

    public partial class FrmReport : Form
    {
        Slab slabw;
        bool useAsw;
        DataGridView dataGV_Report_Asw;
        Label lb_second;
        List<PerimetrSteel> perimsInfo;
        object byLoadObj;
        public FrmReport(Slab _slabw, bool _use_asw, object _byLoadObj)
        {
            InitializeComponent();
            slabw=_slabw;
            byLoadObj = _byLoadObj;
            useAsw = _use_asw;
            this.btnHideAsw.Enabled = false;
            this.btnHideAsw.Visible = false;
        }

        private void FrmReport_Load(object sender, EventArgs e)
        {
            DataGridSlabConcrete slabReport = new DataGridSlabConcrete(this.dataGV_Report);
            //для лейблов с размерами элементов
            this.lbAcol.Text = ContentDataSlab._sizeCol.Width.ToString()+" мм";
            this.lbBcol.Text = ContentDataSlab._sizeCol.Height.ToString() + " мм";
            this.lbHpl.Text = ContentDataSlab._thicknessSlab.ToString() + " мм";
            if (ContentDataSlab._thicknessCap == 0)
            {
                this.lbLcap.Visible = false;
                this.lbHcap.Visible = false;
                label6.Visible = false;
                label4.Visible = false;
            }      
            this.lbLcap.Text = ((ContentDataSlab._capCol.Height - ContentDataSlab._sizeCol.Height) * 0.5).ToString() + " мм";
            this.lbHcap.Text = ContentDataSlab._thicknessCap.ToString() + " мм";

            string messg=string.Empty;
            int answ = slabw.checkVsd_Vrdc();
            switch (answ)
            {
                case 1: messg = "Условие прочности плиты без Asw обеспечено!";break;
                case 0: messg = "Условие прочности плиты без Asw не обеспечено!\nРекомендации:\n1) увеличить толщину плиты.\n2) добавить капитель.\n3) добавить Asw" ;
                        break;
                case -1: messg = "Условие прочности плиты без Asw не обеспечено!\nРекомендации:\n1) увеличить толщину плиты.\n2) добавить капитель."; break;
                case 2: messg = "Условие прочности плиты без Asw не обеспечено!\nРекомендации:\n1) увеличте ширину капители"; break;
                case 3: messg = "Условие прочности плиты без Asw не обеспечено!\nРекомендации:\n1) установить Asw в капитель.\n2) увеличить толщину капители"; break;
                case 4: messg = "Условие прочности плиты без Asw не обеспечено!\nРекомендации:\n1) увеличить ширину капители."; break;
                case 5: messg = "Условие прочности плиты без Asw не обеспечено!\nРекомендации:\n1) увеличить толщину и ширину капители."; break;
                case 6: messg = "Условие прочности плиты без Asw не обеспечено!\nРекомендации:\n1) увеличить толщину."; break;
            }
            //выставляем расположение группбокса с размерами элементов в зависимости от вида расчета(авто или ручной)
            if (byLoadObj.GetType() == typeof(FrmAutoCalc) && answ != 1)
            {
                messg = "Условие прочности плиты\nОБЕСПЕЧЕНО ТОЛЬКО с Asw";
            }

            string param1, param2;
            double vrdcex_vsdex = Math.Round((ContentDataSlab._VRdc_ex / ContentDataSlab._Vsd_ex), 4);
            double vrdcin_vsdin = Math.Round((ContentDataSlab._VRdc_in / (ContentDataSlab._Vsd_in == 0 ? 1 : ContentDataSlab._Vsd_in)),4);

            if (ContentDataSlab._thicknessCap == 0)
            { param1 = "Vrdc"; param2 = "Vsd"; }
            else
            {param1 = "Vrdc_ex"; param2 = "Vsd_ex"; }

            slabReport.AddNewRow(param1, ContentDataSlab._VRdc_ex.ToString(), "Т/м", "-", Color.White);
            slabReport.AddNewRow(param2, ContentDataSlab._Vsd_ex.ToString(), "Т/м", "-", Color.White);
            slabReport.AddNewRow(param1 + "/" + param2, "-", "-", vrdcex_vsdex.ToString(), vrdcex_vsdex>1?Color.Green:Color.Red);

            if(ContentDataSlab._Vsd_in!=0)
            {
                slabReport.AddNewRow("Vrdc_in", ContentDataSlab._VRdc_in.ToString(), "Т/м", "-", Color.White);
                slabReport.AddNewRow("Vsd_in", ContentDataSlab._Vsd_in.ToString(), "Т/м", "-", Color.White);
                slabReport.AddNewRow("Vrdc_in/Vsd_in", "-", "-", vrdcin_vsdin.ToString(), vrdcin_vsdin > 1 ? Color.Green : Color.Red);
            }

            if (ContentDataSlab._thicknessCap == 0)
                param1 = "Ucrit";
            else
                param1 = "Ucrit_ex";
            slabReport.AddNewRow(param1, ContentDataSlab._Ucrit_ex.ToString(), "мм", "-", Color.White);

            if (ContentDataSlab._Vsd_in != 0)
            { slabReport.AddNewRow("Ucrit_in", ContentDataSlab._Ucrit_in.ToString(), "мм", "-", Color.White); }

            slabReport.AddNewRow("d", ContentDataSlab._wHeight.ToString(), "мм", "-",Color.White);
            slabReport.AddNewRow("Rol(коэф. арм.)", ContentDataSlab._PL.ToString(), "-", "-", Color.White);
            this.rTB_report.Text = messg;
            if (useAsw)
                rTB_report.BackColor = Color.Yellow;
            if (answ == 1)
                rTB_report.BackColor = Color.Green;
            if (answ != 1 && !useAsw)
                rTB_report.BackColor = Color.Tomato;
            if (useAsw&&(answ==3||answ==0))
            {
                if (slabw.checkVsd_0() == 0)
                {
                    MessageBox.Show("Установить Asw невозможно! Прочность плиты на раздавливание не обеспечена!", "ВНИМАНИЕ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                perimsInfo= this.slabw.Auto_check();

                //создаем lable
                lb_second = new Label();
                lb_second.Text="Армирование расчетных периметров Asw";
                lb_second.Location = new Point(this.rTB_report.Location.X, this.rTB_report.Location.Y + this.rTB_report.Size.Height + 5);
                lb_second.AutoSize = true;
                lb_second.Size = new System.Drawing.Size(200, 15);
                //создаем GreedView
                int x_GV_Asw = this.rTB_report.Location.X;
                int y_GV_Asw = this.rTB_report.Location.Y + this.rTB_report.Size.Height + 25;
                
                this.dataGV_Report_Asw = new DataGridView();
                this.dataGV_Report_Asw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                this.dataGV_Report_Asw.Location = new System.Drawing.Point(x_GV_Asw, y_GV_Asw);
                this.dataGV_Report_Asw.Name = "dataGV_Report_Asw";
                this.dataGV_Report_Asw.Size = new System.Drawing.Size(475, 45+20*perimsInfo.Capacity);
                this.dataGV_Report_Asw.TabIndex = 0;
                this.btnOk.Location = new Point(this.btnOk.Location.X, this.btnOk.Location.Y + this.dataGV_Report_Asw.Size.Height+20);
                this.btnShowAsw.Location = new Point(this.btnShowAsw.Location.X, this.btnShowAsw.Location.Y + this.dataGV_Report_Asw.Size.Height + 20);
                this.btnShowAsw.Visible = true;
                this.btnHideAsw.Visible = true;
                this.btnHideAsw.Location = new Point(this.btnHideAsw.Location.X, this.btnHideAsw.Location.Y + this.dataGV_Report_Asw.Size.Height + 20);
                this.ClientSize = new Size(this.ClientSize.Width,dataGV_Report_Asw.Size.Height+this.ClientSize.Height+20);
                //добавляем элементы на форму
                this.Controls.Add(this.dataGV_Report_Asw);
                this.Controls.Add(this.lb_second);

                //создаем и заполняем таблицу
                DataGridSlabAsw DGVW_Asw = new DataGridSlabAsw(this.dataGV_Report_Asw);
                int n=1;
                foreach (PerimetrSteel tempSteel in perimsInfo)
                {
                    string t=(n==perimsInfo.Capacity)?"Ua":"U"+n.ToString();
                    DGVW_Asw.AddNewRow(t, tempSteel._numb_elements.ToString(),tempSteel._length_per.ToString(), tempSteel._Vrd_sy.ToString(), tempSteel._Vsd.ToString());
                    n++;
                }
            }
        }

        private void dataGV_Report_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnShowAsw_Click(object sender, EventArgs e)
        { 
            PictureBox pBoxReport= new PictureBox();
            pBoxReport.BorderStyle = BorderStyle.Fixed3D;
            pBoxReport.Location= new Point(500,5);
            pBoxReport.Size= new Size(400,400);
            pBoxReport.BackColor= Color.White;
            this.ClientSize = new Size(this.ClientSize.Width+420, this.ClientSize.Height);
            this.Location = new Point(this.Location.X-210, this.Location.Y);
            this.Controls.Add(pBoxReport);
            double scale = (ContentDataSlab._posColumn==position.corner?1.6:1.0)*0.90 * (pBoxReport.Size.Height - perimsInfo.Capacity * 30) / Math.Max(this.perimsInfo[perimsInfo.Capacity - 1]._Swtot * 2 + ContentDataSlab._sizeCol.Width,
                this.perimsInfo[perimsInfo.Capacity - 1]._Swtot * 2 + ContentDataSlab._sizeCol.Height);
            DrawReport graphReport= new DrawReport(this.perimsInfo,pBoxReport);
            graphReport.Draw(scale);
            this.btnShowAsw.Enabled = false;
            this.btnHideAsw.Enabled = true;
        }

        private void btnHideAsw_Click(object sender, EventArgs e)
        {
            this.ClientSize = new Size(this.ClientSize.Width - 420, this.ClientSize.Height);
            this.Location = new Point(this.Location.X + 210, this.Location.Y);
            this.btnShowAsw.Enabled = true;
            this.btnHideAsw.Enabled = false;   
        }
                
    }
    public class DataGridSlabConcrete
    {
        DataGridView gridviews;
        public DataGridSlabConcrete(DataGridView obj)
        {
            gridviews = obj;
            gridviews.RowHeadersVisible = false;
            var column1 = new DataGridViewColumn();
            column1.HeaderText = "Параметр";
            column1.Width = 120;
            column1.ReadOnly = true;
            column1.Name = "dGvwParam";
            column1.Frozen = true;
            column1.CellTemplate = new DataGridViewTextBoxCell();
            var column2 = new DataGridViewColumn();
            column2.Width = 110;
            column2.HeaderText = "Значение";
            column2.ReadOnly = true;
            column2.Name = "dGvwValue";
            column2.Frozen = true;
            column2.CellTemplate = new DataGridViewTextBoxCell();
            var column3 = new DataGridViewColumn();
            column3.Width = 110;
            column3.HeaderText = "Ед.изм.";
            column3.ReadOnly = true;
            column3.Name = "dGvwUnits";
            column3.Frozen = true;
            column3.CellTemplate = new DataGridViewTextBoxCell();
            var column4 = new DataGridViewColumn();
            column4.Width = 115;
            column4.HeaderText = "Коэфф. запаса";
            column4.ReadOnly = true;
            column4.Name = "dGvwCoeff";
            column4.Frozen = true;
            column4.CellTemplate = new DataGridViewTextBoxCell();
            this.gridviews.Columns.Add(column1);
            this.gridviews.Columns.Add(column2);
            this.gridviews.Columns.Add(column3);
            this.gridviews.Columns.Add(column4);
            this.gridviews.AllowUserToAddRows = false;
        }
        public void AddNewRow(string Param, string Value, string Units, string Coff, Color b_color)
        {
            gridviews.Rows.Add();
            gridviews[0, gridviews.Rows.Count - 1].Value = Param;
            gridviews[1, gridviews.Rows.Count - 1].Value = Value;
            gridviews[2, gridviews.Rows.Count - 1].Value = Units;
            gridviews[3, gridviews.Rows.Count - 1].Value = Coff;
            gridviews.Rows[gridviews.Rows.Count - 1].DefaultCellStyle.BackColor = b_color;
        }
    }

    public class DataGridSlabAsw
    {
        DataGridView gridviews;
        public DataGridSlabAsw(DataGridView obj)
        {
            gridviews = obj;
            gridviews.RowHeadersVisible = false;
            var column1 = new DataGridViewColumn();
            column1.HeaderText = "Номер периметра";
            column1.Width = 75;
            column1.ReadOnly = true;
            column1.Name = "dGvwNumbPerim";
            column1.Frozen = true;
            column1.CellTemplate = new DataGridViewTextBoxCell();
            var column2 = new DataGridViewColumn();
            column2.HeaderText = "Кол. стержней";
            column2.Width = 75;
            column2.ReadOnly = true;
            column2.Name = "dGvwNumb";
            column2.Frozen = true;
            column2.CellTemplate = new DataGridViewTextBoxCell();
            var column3 = new DataGridViewColumn();
            column3.HeaderText = "Длина перим.";
            column3.Width = 110;
            column3.ReadOnly = true;
            column3.Name = "dGvwLengthPer";
            column3.Frozen = true;
            column3.CellTemplate = new DataGridViewTextBoxCell();
            var column4 = new DataGridViewColumn();
            column4.HeaderText = "Vrd,si (Т/м)";
            column4.Width = 110;
            column4.ReadOnly = true;
            column4.Name = "dGvwVrdsi";
            column4.Frozen = true;
            column4.CellTemplate = new DataGridViewTextBoxCell();
            var column5 = new DataGridViewColumn();
            column5.HeaderText = "Vsd,si (Т/м)";
            column5.Width = 100;
            column5.ReadOnly = true;
            column5.Name = "dGvwVsdsi";
            column5.Frozen = true;
            column5.CellTemplate = new DataGridViewTextBoxCell();
            this.gridviews.Columns.Add(column1);
            this.gridviews.Columns.Add(column2);
            this.gridviews.Columns.Add(column3);
            this.gridviews.Columns.Add(column4);
            this.gridviews.Columns.Add(column5);
            this.gridviews.AllowUserToAddRows = false;
        }
        public void AddNewRow(string NumbPer, string Value, string PerimLength, string Vrd_si, string Vsd_si)
        {
            gridviews.Rows.Add();
            gridviews[0, gridviews.Rows.Count - 1].Value = NumbPer;
            gridviews[1, gridviews.Rows.Count - 1].Value = Value;
            gridviews[2, gridviews.Rows.Count - 1].Value = PerimLength;
            gridviews[3, gridviews.Rows.Count - 1].Value = Vrd_si;
            gridviews[4, gridviews.Rows.Count - 1].Value = Vsd_si;
        }
    }
}
